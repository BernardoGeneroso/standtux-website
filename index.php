<?php
  /**
   * Página inicial
   */
  $header = "/";

  include_once('./db/db.php');
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSS & Icon  -->
    <link rel="stylesheet" href="./assets/css/global.css">
    <link rel="stylesheet" href="./assets/css/home/index.css">
    <link rel="stylesheet" href="./assets/css/toastr/toastr.min.css">
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="icon" href="./assets/img/logos/siteicon.ico" type="image/x-icon">

    <!-- Website title -->
    <title>Stand Tux</title>
  </head>
  <body>
    <!-- NavBar - Header -->

    <?php include './components/navbar.php';
      /**
       * Mostra as informações do utilizador caso a sessão estiver iniciada
       */
      if (isset($_SESSION['user_id'])){
        $id_user = $_SESSION['user_id'];
      }
    ?>

    <!-- Carousel  --->
    <div id="carouselHomePage" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">
        <li data-target="#carouselHomePage" data-slide-to="0" class="active"></li>
        <li data-target="#carouselHomePage" data-slide-to="1"></li>
        <li data-target="#carouselHomePage" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner">
        <div class="carousel-item active" data-interval="8000">
          <img src="./assets/img/home/carousel/img1.jpg" class="d-block w-100 carouselImg" alt="img1">
        </div>
        <div class="carousel-item" data-interval="8000">
          <img src="./assets/img/home/carousel/img2.jpg" class="d-block w-100 carouselImg" alt="img2">
        </div>
        <div class="carousel-item" data-interval="8000">
          <img src="./assets/img/home/carousel/img3.jpg" class="d-block w-100 carouselImg" alt="img3">
        </div>
      </div>
      <a class="carousel-control-prev" href="#carouselHomePage" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Atrás</span>
      </a>
      <a class="carousel-control-next" href="#carouselHomePage" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Próximo</span>
      </a>
    </div>

    <!-- Informações do Home -->
    <div class="information">

      <!-- Qualidades -->
      <div class="information--container">
        <div class="information--qualities">
          <div class="information--content">
              <i class="fa fa-users" aria-hidden="true"></i>
              <h3>Profissionalismo</h3>
              <p>A equipa tem 20 anos de experiência na área</p>
          </div>
          <div class="information--content">
              <i class="fa fa-star" aria-hidden="true"></i>
              <h3>Qualidade</h3>
              <p>Certificamo-nos que entregamos o melhor ao consumidor</p>
          </div>
          <div class="information--content">
              <i class="fa fa-cog" aria-hidden="true"></i>
              <h3>Suporte</h3>
              <p>Fornecemos suporte de 24/7 de qualquer dúvida que tenha</p>
          </div>
        </div>
      </div>

      <!-- About Us -->
      <div class="aboutus">
        <h1>Sobre nós</h1>

        <div class="aboutus--container">
          <img src="./assets/img/home/aboutus/img.png" alt="AboutUs">

          <div class="aboutus--right">
            <span>
              Estamos neste ramos à 20 anos, e fazêmo-lo á nossa maneira... Apenas vendemos motorizadas e peças para as mesma.
            </span>
            <span>
              Gostámos de um bom modelo clássico, e duma uma boa aventura, não consideramos um trabalho mas sim um passa-tempo!
            </span>
          </div>
        </div>
      </div>


      <!-- Comments - Carousel -->
      <div class="comments">
        <h1>Comentários</h1>

        <div id="carouselHomePageComments" class="carousel slide" data-ride="carousel">
          <div class="carousel-inner">
            <?php
             /**
              * Apresenta os comentários aprovados em carrousel
              */
              function approvedComments($row, $counter){
                $active = "";
                $starsContent = '';
                if ($counter == 0){
                  $active = "active";
                }
                for ($x = 0; $x < $row[1]; $x++) {
                  $starsContent = $starsContent.'<i class="fa fa-star"></i>';
                }
                echo '
                <div class="carousel-item '.$active.'" data-interval="10000">
                  <div class="comments--container">
                    <div class="comments--text">
                      <p>"'.$row[0].'"</p>
                    </div>
                    <h3>'.$row[2].'</h3>
                    <hr/>
                    <div class="comments--stars">
                      '.$starsContent.'
                    </div>
                  </div>
                </div>';
              }
              $sql = "SELECT r.description, r.rating, u.name FROM rating AS r
                      INNER JOIN user AS u
                      ON u.id_u=r.id_user
                      WHERE approved=1";

              /**
               * Comentários aprovados
               */
              $result = $conn->query($sql);
    
              if ($row_cnt = $result->num_rows) {
                $counter = 0;
                while ($row = mysqli_fetch_row($result)) {
                  approvedComments($row, $counter);
                  $counter = $counter + 1;
                }
              }
            ?>
          </div>
          <a class="carousel-control-prev icon-comment" href="#carouselHomePageComments" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon-comment fa fa-arrow-left" aria-hidden="true"></span>
            <span class="sr-only">Atrás</span>
          </a>
          <a class="carousel-control-next icon-comment" href="#carouselHomePageComments" role="button" data-slide="next">
            <span class="carousel-control-next-icon-comment fa fa-arrow-right" aria-hidden="true"></span>
            <span class="sr-only">Próximo</span>
          </a>
        </div>
      </div>
    </div>

    <!-- Footer -->
    <?php include './components/footer.php'; ?>

    <div>
      <i class="fa fa-arrow-up arrowUp" aria-hidden="true"></i>
    </div>

    <?php
      /**
       * Mostra as informações do utilizador e a lista de desejos caso a sessão estiver iniciada
       */
      if (isset($_SESSION['user_id'])){
        $id_user = $_SESSION['user_id'];
        echo '
        <!-- Wish list -->
        <div class="modal fade" id="wishListMotorized" tabindex="-1" role="dialog" aria-labelledby="wishListMotorizedModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="wishListMotorizedModalLabel">Lista de desejos</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">';
                  $sql = "SELECT fm.id_fav_motorized, m.name, m.image, m.price, m.stock
                          FROM fav_motorized AS fm
                          INNER JOIN motorized AS m
                          ON m.id_motorized=fm.id_motorized
                          WHERE id_user='$id_user'
                          ";
    
                  $result = $conn->query($sql);
    
                    if ($row_cnt = $result->num_rows) {
                      echo '<table class="table table-striped">
                      <thead>
                        <tr>
                          <th scope="col">Nome</th>
                          <th scope="col">Imagem</th>
                          <th scope="col">Preço</th>
                          <th scope="col">Stock</th>
                          <th scope="col">Remover</th>
                        </tr>
                      </thead>
                      <tbody id="listWishMotorized">';
                      while ($row = mysqli_fetch_row($result)) {
                        if ($row[4]  >= 1) {
                          $row[4] = "Disponível";
                        } else {
                          $row[4] = "Esgotado";
                        }
                        echo '
                        <tr class="content" data-id="'.$row[0].'">
                          <td>'.$row[1].'</td>
                          <td><img src="'.$row[2].'" alt="'.$row[1].'"></td>
                          <td>'.$row[3].'</td>
                          <td>'.$row[4].'</td>
                          <td id="removeWishListMotorized">
                            <i class="fa fa-times"></i>
                          </td>
                        </tr>
                        ';
                      }
                      echo '</tbody>
                      </table>';
                    } else {
                      echo '<div style="text-align:center;">Nenhuma motorizada na lista de desejos</div>';
                    }
                
              echo '</div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
              </div>
            </div>
          </div>
        </div>';
    
        echo '<!-- Perfil Menu -->
        <div class="modal fade" id="perfilMenu" tabindex="-1" role="dialog" aria-labelledby="perfilMenuModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="perfilMenuModalLabel">Perfil</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="perfil-user-image">
                    <input type="file" id="file-profile-user">
    
                    <img src="'.$_SESSION['user_image'].'" id="image-profile-user" alt="'.$_SESSION['user_name'].'">
                
                    <div id="error-message-img-profile" style="display:none;">
                      <p></p>
                    </div>
                </div>
    
                <form>
                  <div class="form-row">
                    <div class="form-group col-md-6">
                      <label for="inputNamePerfil">Nome</label>
                      <input type="text" class="form-control" id="inputNamePerfil" value="'.$_SESSION['user_name'].'" placeholder="Nome e apelido">
                    </div>
                    <div class="form-group col-md-6">
                      <label for="inputEmailPerfil">Email</label>
                      <input type="email" class="form-control" id="inputEmailPerfil" value="'.$_SESSION['user_email'].'" placeholder="Email">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputAddress">Endereço</label>
                    <input type="text" class="form-control" id="inputAddress" value="'.$_SESSION['user_address'].'" placeholder="Endereço">
                  </div>
                  <div class="form-row">
                    <div class="form-group col-md-4">
                      <label for="inputCity">Cidade</label>
                      <input type="text" class="form-control" id="inputCity" value="'.$_SESSION['user_city'].'" placeholder="Cidade">
                    </div>
                    <div class="form-group col-md-4">
                      <label for="inputNumberPhone">Número telemóvel</label>
                      <input type="text" class="form-control" id="inputNumberPhone" value="'.$_SESSION['user_mobile_phone'].'" maxlength="9" placeholder="Número telemóvel">
                    </div>
                    <div class="form-group col-md-2">
                      <label for="inputNIF">NIF</label>
                      <input type="text" class="form-control" id="inputNIF" value="'.$_SESSION['user_nif'].'" maxlength="9" placeholder="Contribuinte">
                    </div>
                    <div class="form-group col-md-2">
                      <label for="inputPostalCode">Código postal</label>
                      <input type="text" class="form-control" id="inputPostalCode" value="'.$_SESSION['user_postal_cod'].'" maxlength="8" placeholder="0000-000">
                    </div>
                  </div>
                </form>
                
                <button type="button" class="btn btn-success" id="saveDataProfileUser">Guardar alterações</button>';
    
                $sql = "SELECT id_rating FROM rating WHERE id_user='$id_user'";
    
                $result = $conn->query($sql);
    
                if (!$row_cnt = $result->num_rows) {
                  echo '<hr style="height:2px;border-width:0;background-color:whitesmoke">
                  <div class="profile-rating-stars">
                    <i data-position="1" class="fa fa-star"></i>
                    <i data-position="2" class="fa fa-star"></i>
                    <i data-position="3" class="fa fa-star"></i>
                    <i data-position="4" class="fa fa-star"></i>
                    <i data-position="5" class="fa fa-star"></i>
                  </div>
                   
                   <div class="comment-home-profile-user">
                      <div class="input-group mb-3">
                        <input type="text" class="form-control" placeholder="Comentário sobre a Stand Tux...">
                        <div class="input-group-append">
                          <button class="btn btn-outline-secondary" type="button" id="sendCommentProfileUser">Enviar</button>
                        </div>
                      </div>
                      <p style="display:none;"></p>
                    </div>';
                }
    
              echo '</div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
              </div>
            </div>
          </div>
        </div>';
      }
    ?>

    <!-- Script's load -->
    <script src="./assets/js/jquery-3.5.1.min.js"></script>
    <script src="./assets/js/navbar/script.js"></script>
    <script src="./assets/js/home/script.js"></script>
    <script src="./assets/js/toastr/toastr.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>  
  </body>
</html>