<?php
  if(isset($_POST['update']) && $_POST['update'] == 1){
    session_start();

    include_once('../db/db.php');

    $id_user = $_SESSION['user_id'];
    $cart = [];

    $sql = "SELECT m.id_motorized, m.name, m.image FROM cart_motorized as cm, motorized as m WHERE cm.id_user='$id_user' AND m.id_motorized = cm.id_motorized";

    $result = $conn->query($sql);

    if ($row_cnt = $result->num_rows) {
      while ($row = mysqli_fetch_row($result)) {
        array_push($cart, $row);
      }
    }

    echo json_encode($cart);
  }
?>