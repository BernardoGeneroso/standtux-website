<?php
  if(isset($_POST['update']) && $_POST['update'] == 1){
    session_start();

    include_once('../db/db.php');

    $email = trim($conn->real_escape_string($_POST['email']));
    $password = hash("sha512",trim($conn->real_escape_string($_POST['password'])));

    $sql = "SELECT id_u, name, image, admin, address, nif, postal_cod, city, mobile_phone FROM user WHERE email = '$email' AND password = '$password'";
  
    $result = $conn->query($sql);

    if ($row_cnt = $result->num_rows) {
      while ($row = mysqli_fetch_row($result)) {
        $_SESSION['user_id'] = $row[0];
        $_SESSION['user_name'] = $row[1];
        $_SESSION['user_email'] = $email;
        $_SESSION['user_image'] = $row[2];
        $_SESSION['user_admin'] = $row[3];
        $_SESSION['user_address'] = $row[4] ? $row[4] : '';
        $_SESSION['user_nif'] = $row[5] ? $row[5] : '';
        $_SESSION['user_postal_cod'] = $row[6] ? $row[6] : '';
        $_SESSION['user_city'] = $row[7] ? $row[7] : '';
        $_SESSION['user_mobile_phone'] = $row[8] ? $row[8] : '';
        echo json_encode(array(1));
      }
    } else {
      $return = array(0, 'Credenciais incorrectas');
      echo json_encode($return);
    }
  }
?>