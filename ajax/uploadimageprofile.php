<?php
  if(isset($_FILES['file']) && !empty($_FILES['file'])){
    session_start();

    include_once('../db/db.php');

    $id_user = $_SESSION['user_id'];
    $file = $_FILES['file'];

    $fileTmpName = $_FILES['file']['tmp_name'];
		$fileName = $_FILES['file']['name'];

    $fileExt = explode('.',$fileName);
		$fileActualExt = strtolower(end($fileExt));

    $fileNameNew = uniqid('',true).".".$fileActualExt;

    $fileDestination = '../assets/img/uploads/avatar/'.$fileNameNew;
    $fileDestinationSend = './assets/img/uploads/avatar/'.$fileNameNew; 

    move_uploaded_file($fileTmpName, $fileDestination);

    $sql = "UPDATE user SET image='$fileDestination' WHERE id_u='$id_user'";

    $conn->query($sql);

    $_SESSION['user_image'] = $fileDestinationSend;

    echo json_encode($fileDestinationSend, JSON_UNESCAPED_UNICODE);
  }
?>