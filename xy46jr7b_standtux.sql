-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Tempo de geração: 23-Jan-2021 às 19:24
-- Versão do servidor: 5.6.49-cll-lve
-- versão do PHP: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `xy46jr7b_standtux`
--
CREATE DATABASE IF NOT EXISTS `xy46jr7b_standtux` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `xy46jr7b_standtux`;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cart_motorized`
--

CREATE TABLE `cart_motorized` (
  `id_cart_motorized` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_motorized` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `cart_motorized`
--

INSERT INTO `cart_motorized` (`id_cart_motorized`, `id_user`, `id_motorized`) VALUES
(4, 1, 1),
(12, 2, 1),
(21, 4, 9);

-- --------------------------------------------------------

--
-- Estrutura da tabela `comment_motorized`
--

CREATE TABLE `comment_motorized` (
  `id_comment` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_motorized` int(11) NOT NULL,
  `comment` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `comment_motorized`
--

INSERT INTO `comment_motorized` (`id_comment`, `id_user`, `id_motorized`, `comment`) VALUES
(1, 4, 1, 'Uma sensação de condução fantástica, deu para tirar as saudades!'),
(4, 2, 1, 'Adorei conduzir');

-- --------------------------------------------------------

--
-- Estrutura da tabela `comment_piece`
--

CREATE TABLE `comment_piece` (
  `id_comment` int(11) NOT NULL,
  `id_piece` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `comment` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `fav_motorized`
--

CREATE TABLE `fav_motorized` (
  `id_fav_motorized` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_motorized` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `fav_motorized`
--

INSERT INTO `fav_motorized` (`id_fav_motorized`, `id_user`, `id_motorized`) VALUES
(1, 1, 1),
(8, 2, 1),
(13, 4, 1),
(9, 8, 11),
(10, 8, 13);

-- --------------------------------------------------------

--
-- Estrutura da tabela `fav_piece`
--

CREATE TABLE `fav_piece` (
  `id_fav_piece` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_piece` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `motorized`
--

CREATE TABLE `motorized` (
  `id_motorized` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `brand` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(120) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'https://i.pinimg.com/originals/0a/bc/ae/0abcae7d29fdfb484897b86669e6d150.jpg',
  `price` decimal(10,2) NOT NULL,
  `promotion` decimal(10,0) NOT NULL DEFAULT '0',
  `fuel_capacity` decimal(10,2) NOT NULL,
  `year_manufacturing` int(4) NOT NULL,
  `power` decimal(10,2) NOT NULL,
  `cylinder` int(3) NOT NULL,
  `weight` int(81) NOT NULL,
  `stock` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `motorized`
--

INSERT INTO `motorized` (`id_motorized`, `name`, `brand`, `image`, `price`, `promotion`, `fuel_capacity`, `year_manufacturing`, `power`, `cylinder`, `weight`, `stock`, `active`, `description`) VALUES
(1, 'Honda AC15', 'Honda', './assets/img/motorized/motorizadas/honda_ac_15.jpg', 5589.15, 10, 8.50, 1997, 4.20, 50, 81, 51, 1, 'O  estilo dos anos 1960, havia o guiador de corrida curto e um tanque de combustível alongado e discreto. A estrutura era um desenho de diamante com um tubo frontal reforçado para baixo aparafusado ao cárter do motor.'),
(9, 'Lambretta Model D', 'Lambretta', './assets/img/motorized/motorizadas/lambretta_150d.jpg', 8945.04, 0, 6.60, 1955, 4.50, 148, 75, 21, 1, 'Fabricado pelo gigante industrial italiano Innocenti, a Lambretta ganhou aceitação instantânea nos anos do pós-guerra imediato, principalmente por causa de sua limpeza e conveniência.'),
(10, 'Lambretta Li 125cc Series 3', 'Lambretta', './assets/img/motorized/motorizadas/lambretta_li_1963.jpg', 5006.40, 16, 6.60, 1962, 3.80, 150, 104, 33, 1, 'Originalmente registrada em 28 de março de 2002 como uma Harley-Davidson, esta moto customizada é uma Harley-Davidson moderna com a aparência icônica britânica do Norton e sendo o modelo XL883R tem um motor 883 em seu núcleo.'),
(11, 'Zündapp Bella', 'Zundapp', './assets/img/motorized/motorizadas/zundapp_bella_r_154.jpg', 3367.34, 0, 7.20, 1954, 7.50, 146, 146, 87, 1, 'Uma versão do Bella chamada Suburbanette foi feita para o mercado dos Estados Unidos de 1953 a 1954. O Suburbanette foi despojado dos painéis da carroceria que envolviam o motor.'),
(12, 'Jawa 350Automatic', 'Jawa', './assets/img/motorized/motorizadas/jawa_350.jpg', 2609.00, 0, 13.50, 1964, 13.30, 350, 76, 5, 1, 'Em 1963, Jawa desenvolveu o primeiro modelo deste conteúdo no mundo com um motor que possuía uma embreagem centrífuga automática que proporcionava maior conforto ao andar de motorizada.'),
(13, 'Hercules K 105 X', 'Hercules', './assets/img/motorized/motorizadas/hercules_k105.jpg', 1600.00, 25, 13.00, 1970, 10.10, 98, 112, 2, 1, 'Manufactured from 1969 to 1971, the bike had an engine with a displacement of 98cc and provided 12HP.'),
(14, 'Macal Thunder', 'Macal', './assets/img/motorized/motorizadas/macal_thunder.jpg', 3241.00, 0, 13.50, 1950, 12.30, 50, 50, 10, 1, 'Foi criada em 1950, esta moto tem 50cc e pesa 50 kg '),
(15, 'Casal S170', 'Casal', './assets/img/motorized/motorizadas/casal_s170.jpg', 3000.00, 0, 5.23, 1945, 6.30, 50, 80, 12, 1, 'Esta motorizada foi criado em 1945, com 50cc e pesa 80 kg '),
(16, 'Honda Dream 50 ', 'Honda', './assets/img/motorized/motorizadas/honda_dream_50.jpg', 2500.00, 0, 7.80, 1991, 15.00, 50, 100, 3, 1, 'A motorizada Honda Dream 50 foi criada 1991, com 50cc e pesa 100 kg'),
(17, 'Casal 5 K191   ', 'Casal', './assets/img/motorized/motorizadas/casal_5_K191.jpg', 5263.00, 0, 15.00, 1971, 5.00, 50, 90, 5, 1, 'A Casal 5 K191 foi criada 1971, com 50cc e pesa 90kg.'),
(18, 'Kreidler Florett', 'Honda', './assets/img/motorized/motorizadas/kreidler_florett.jpg', 6532.00, 0, 8.33, 1961, 8.00, 50, 96, 13, 1, 'A Kreidler Florett foi criada 1961, com 50cc e pesa 96kg.'),
(19, 'Sachs Minor GT', 'Sachs', './assets/img/motorized/motorizadas/sachs_minor_gt.jpg', 4652.00, 0, 10.00, 1980, 5.30, 50, 120, 20, 1, 'A Sachs Minor GT foi criada 1980 , com 50cc e pesa 120kg'),
(20, 'Famel xf21', 'Famel', './assets/img/motorized/motorizadas/famel_xf21.jpg', 8023.00, 0, 11.23, 1988, 7.36, 50, 96, 13, 1, 'Esta motorizadas foi criada 1988 , com 50cc e pesa 96.'),
(28, 'aa', 'Teamspeak', 'https://i.pinimg.com/originals/0a/bc/ae/0abcae7d29fdfb484897b86669e6d150.jpg', 20000.00, 0, 15.00, 2000, 35.00, 91, 90, 34, 0, 'nadafewfew'),
(29, 'Brixton BX 125', 'Brixton', 'https://i.pinimg.com/originals/0a/bc/ae/0abcae7d29fdfb484897b86669e6d150.jpg', 5299.00, 0, 12.00, 2019, 8.10, 124, 120, 12, 1, 'Mota muita fixe'),
(30, 'Demak Duta', 'Demak', 'https://i.pinimg.com/originals/0a/bc/ae/0abcae7d29fdfb484897b86669e6d150.jpg', 1700.00, 0, 7.00, 2013, 6.00, 125, 100, 76, 1, 'Mota da Barbie'),
(31, 'Durkopp Diana TS', 'Durkopp', 'https://i.pinimg.com/originals/0a/bc/ae/0abcae7d29fdfb484897b86669e6d150.jpg', 8955.00, 0, 12.00, 1961, 7.90, 171, 108, 12, 1, 'Uma mota muito bonita, assim como todas as outras do nosso website'),
(32, 'Haojin JH125 E', 'Haojin', 'https://i.pinimg.com/originals/0a/bc/ae/0abcae7d29fdfb484897b86669e6d150.jpg', 3499.00, 2, 13.00, 2010, 6.90, 124, 99, 3, 1, 'Motorizada moderna com cores lindas'),
(33, 'Hercules K125 S', 'Hercules', 'https://i.pinimg.com/originals/0a/bc/ae/0abcae7d29fdfb484897b86669e6d150.jpg', 5500.00, 0, 11.00, 1979, 12.40, 124, 112, 6, 1, 'Motorizada antiga porém em ótimo estado'),
(34, 'Jawa 640 Classic', 'Jawa', 'https://i.pinimg.com/originals/0a/bc/ae/0abcae7d29fdfb484897b86669e6d150.jpg', 4440.00, 0, 9.00, 2001, 17.50, 343, 115, 1, 1, 'Motorizada de 2001, rápida'),
(35, 'Junak M10', 'Junak', 'https://i.pinimg.com/originals/0a/bc/ae/0abcae7d29fdfb484897b86669e6d150.jpg', 6966.00, 10, 12.00, 1961, 13.90, 349, 122, 7, 1, 'A fantástica Junak M10 é uma ótima escolha para qualquer cliente'),
(36, 'James Comet', 'James', 'https://i.pinimg.com/originals/0a/bc/ae/0abcae7d29fdfb484897b86669e6d150.jpg', 5500.00, 0, 11.00, 1958, 12.90, 98, 108, 23, 0, 'A James Comet é uma motorizada de alta qualidade'),
(37, 'Ducati 350 Vento', 'Ducati', 'https://i.pinimg.com/originals/0a/bc/ae/0abcae7d29fdfb484897b86669e6d150.jpg', 3999.00, 0, 14.00, 1981, 11.20, 340, 100, 13, 1, 'A Ducati Vento é a uma motorizada que nós recomendados a qualquer um'),
(38, 'Puch 125 GS', 'Puch', 'https://i.pinimg.com/originals/0a/bc/ae/0abcae7d29fdfb484897b86669e6d150.jpg', 3789.00, 0, 10.00, 1975, 10.20, 123, 101, 33, 1, 'Puch 125 GS é uma das motorizadas mais antigas porém das melhores'),
(39, 'Puch Alpine SR', 'Puch', 'https://i.pinimg.com/originals/0a/bc/ae/0abcae7d29fdfb484897b86669e6d150.jpg', 3290.00, 0, 8.00, 1959, 4.40, 145, 97, 3, 1, 'Uma grande motorizada com muita história'),
(40, 'Puch M125', 'Puch', 'https://i.pinimg.com/originals/0a/bc/ae/0abcae7d29fdfb484897b86669e6d150.jpg', 3400.00, 0, 12.00, 1970, 9.10, 123, 100, 34, 1, 'Outra Puch e outra fantástica escolha de motorizada'),
(41, 'Puch SRA 150', 'Puch', 'https://i.pinimg.com/originals/0a/bc/ae/0abcae7d29fdfb484897b86669e6d150.jpg', 3387.00, 0, 10.00, 1960, 4.40, 145, 99, 9, 1, 'Motorizada bela e rápida'),
(42, 'Raybar 200B', 'Raybar', 'https://i.pinimg.com/originals/0a/bc/ae/0abcae7d29fdfb484897b86669e6d150.jpg', 4900.00, 0, 9.80, 2020, 9.80, 200, 97, 23, 1, 'Raybar, uma ótima mota para pessoas de bom gosto'),
(43, 'Rikuo RT2', 'Rikuo', 'https://i.pinimg.com/originals/0a/bc/ae/0abcae7d29fdfb484897b86669e6d150.jpg', 5100.00, 0, 0.00, 1958, 16.10, 750, 134, 5, 0, 'Uma das motorizadas mais potentes disponível na nossa loja'),
(44, 'Rudge Ulster 500', 'Rudge', 'https://i.pinimg.com/originals/0a/bc/ae/0abcae7d29fdfb484897b86669e6d150.jpg', 4125.00, 0, 7.80, 1939, 32.80, 499, 110, 8, 1, 'Uma motorizada muito antiga porém muito potente'),
(45, 'Sparta 250 Twin', 'Sparta', 'https://i.pinimg.com/originals/0a/bc/ae/0abcae7d29fdfb484897b86669e6d150.jpg', 3900.00, 0, 9.90, 1958, 17.50, 244, 102, 15, 1, 'Sparta, uma mota muito boa'),
(46, 'Sparta SL 250', 'Sparta', 'https://i.pinimg.com/originals/0a/bc/ae/0abcae7d29fdfb484897b86669e6d150.jpg', 4000.00, 0, 11.40, 1958, 18.00, 245, 99, 54, 1, 'Muito boa, mota muito muito boa'),
(47, 'Sparta NL200', 'Sparta', 'https://i.pinimg.com/originals/0a/bc/ae/0abcae7d29fdfb484897b86669e6d150.jpg', 3300.00, 0, 8.50, 1955, 16.00, 200, 100, 21, 1, 'Mais outra mota da Sparta para a nossa loja'),
(48, 'Shineray XY-125 11', 'Shineray', 'https://i.pinimg.com/originals/0a/bc/ae/0abcae7d29fdfb484897b86669e6d150.jpg', 3454.00, 20, 0.00, 2011, 7.80, 124, 97, 9, 0, 'Shineray, uma mota muito fixe');

-- --------------------------------------------------------

--
-- Estrutura da tabela `motorized_hire`
--

CREATE TABLE `motorized_hire` (
  `id_motorized_hire` int(11) NOT NULL,
  `id_motorized` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `motorized_hire`
--

INSERT INTO `motorized_hire` (`id_motorized_hire`, `id_motorized`) VALUES
(1, 1),
(2, 11);

-- --------------------------------------------------------

--
-- Estrutura da tabela `order`
--

CREATE TABLE `order` (
  `id_order` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `payment_method` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(25) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Preparação',
  `order_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `order`
--

INSERT INTO `order` (`id_order`, `id_user`, `payment_method`, `status`, `order_date`) VALUES
(1, 4, 'VISA', 'Preparação', '2020-11-18 20:53:50');

-- --------------------------------------------------------

--
-- Estrutura da tabela `piece`
--

CREATE TABLE `piece` (
  `id_piece` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `price` decimal(10,0) NOT NULL,
  `image` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `promotion` decimal(10,0) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `product_order`
--

CREATE TABLE `product_order` (
  `id_product_order` int(11) NOT NULL,
  `id_motorized` int(11) NOT NULL,
  `id_order` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `product_order`
--

INSERT INTO `product_order` (`id_product_order`, `id_motorized`, `id_order`) VALUES
(1, 1, 1),
(2, 9, 1),
(3, 14, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `rating`
--

CREATE TABLE `rating` (
  `id_rating` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rating` int(1) NOT NULL,
  `approved` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `rating`
--

INSERT INTO `rating` (`id_rating`, `id_user`, `description`, `rating`, `approved`) VALUES
(1, 5, 'Gostei muito da interface do site, permitindo-me obter informações acerca da motorizada que pretendia de maneira rápida e eficaz', 5, 1),
(2, 6, 'O site possui todo o tipo diversificado de motas e peças, tendo também preços acessíveis para o mesmos', 5, 0),
(3, 7, 'De imediato fiquei extremamente feliz pelo fácil e eficaz design do site, não só isso como as peças que comprei vieram em perfeita condição e dentro de poucos dias, muito obrigada!', 5, 1),
(4, 3, 'Que site horrível! Eu até gostei, mas continua a não gostar do site. Ai ai ai', 1, 0),
(5, 1, 'Muito bom! Site amigável, boas motorizadas, adorei a vossa história...', 5, 1),
(7, 2, 'Quem fez este site desse ser ganda gajo', 5, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `rental`
--

CREATE TABLE `rental` (
  `id_rental` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_motorized_hire` int(11) NOT NULL,
  `start_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `end_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `price` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `rental`
--

INSERT INTO `rental` (`id_rental`, `id_user`, `id_motorized_hire`, `start_date`, `end_date`, `price`) VALUES
(1, 4, 1, '2020-11-18 21:17:49', '2020-11-27 20:17:28', 128.00);

-- --------------------------------------------------------

--
-- Estrutura da tabela `user`
--

CREATE TABLE `user` (
  `id_u` int(11) NOT NULL,
  `name` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `admin` tinyint(4) NOT NULL,
  `image` varchar(120) COLLATE utf8_unicode_ci DEFAULT './assets/img/logos/avatar.png',
  `address` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nif` int(25) DEFAULT NULL,
  `postal_cod` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile_phone` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `user`
--

INSERT INTO `user` (`id_u`, `name`, `password`, `email`, `admin`, `image`, `address`, `nif`, `postal_cod`, `city`, `mobile_phone`) VALUES
(1, 'Bernardo Generoso', 'be6df9ca3e4af92b631bc23a928ae970cac8bc6adbc9c88bc58d6dcb110987e76d18830b58748602911a5f723d9b76adc031266da63320f541bff01cdffbfcf6', 'bernardo@standtux.pt', 0, './assets/img/logos/avatar.png', NULL, NULL, NULL, NULL, NULL),
(2, 'Diogo Agostinho', '4aeb2000b9de5858f5e5e0b7eda52f253caf19582c67cbbb453be6987ecc1baf27d75670e39f78058fb1ebee3d16b83d1cbdc8d3628636377b2458ea5bf12ff2', 'agostinho@standtux.pt', 0, '../assets/img/uploads/avatar/6008384d900689.99087054.png', 'Estr. da Penha 139', 987654321, '8005-139', 'Faro', '987654321'),
(3, 'Steven Martins', '7fcf4ba391c48784edde599889d6e3f1e47a27db36ecc050cc92f259bfac38afad2c68a1ae804d77075e8fb722503f3eca2b2c1006ee6f6c7b7628cb45fffd1d', 'steven@standtux.pt', 1, './assets/img/logos/avatar.png', NULL, NULL, NULL, NULL, NULL),
(4, 'Admin', '7fcf4ba391c48784edde599889d6e3f1e47a27db36ecc050cc92f259bfac38afad2c68a1ae804d77075e8fb722503f3eca2b2c1006ee6f6c7b7628cb45fffd1d', 'admin@admin.pt', 1, './assets/img/uploads/avatar/600711a0076f06.57448211.png', 'Norte, Distrito de Bragança, Vinhais, Agrochão 3', 123456789, '1234-567', 'Agrochão', '915743254'),
(5, 'João Santos', '7fcf4ba391c48784edde599889d6e3f1e47a27db36ecc050cc92f259bfac38afad2c68a1ae804d77075e8fb722503f3eca2b2c1006ee6f6c7b7628cb45fffd1d', 'joaosantos@standtux.pt', 0, './assets/img/logos/avatar.png', NULL, NULL, NULL, NULL, NULL),
(6, 'Fabiano Silva', '7fcf4ba391c48784edde599889d6e3f1e47a27db36ecc050cc92f259bfac38afad2c68a1ae804d77075e8fb722503f3eca2b2c1006ee6f6c7b7628cb45fffd1d', 'fabianosilva@standtux.pt', 0, './assets/img/logos/avatar.png', NULL, NULL, NULL, NULL, NULL),
(7, 'Mariana Gomes\r\n', '7fcf4ba391c48784edde599889d6e3f1e47a27db36ecc050cc92f259bfac38afad2c68a1ae804d77075e8fb722503f3eca2b2c1006ee6f6c7b7628cb45fffd1d', 'marianagomes@standtux.pt', 0, './assets/img/logos/avatar.png', NULL, NULL, NULL, NULL, NULL),
(8, 'José Lima', '0d574638c92c43a7e4c712f12132d729a96dfda98df39ce9f1d68477fa2647b02529e01cd8b5e2f38dfdc3ade77b63964802b430d5bcb2ad30b32261b3780f9e', 'a67472@ualg.pt', 0, './assets/img/logos/avatar.png', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `wish_list_motorized`
--

CREATE TABLE `wish_list_motorized` (
  `id_wish_list_motorized` int(11) NOT NULL,
  `id_motorized` int(11) NOT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- �?ndices para tabelas despejadas
--

--
-- �?ndices para tabela `cart_motorized`
--
ALTER TABLE `cart_motorized`
  ADD PRIMARY KEY (`id_cart_motorized`),
  ADD KEY `id_motorized` (`id_motorized`),
  ADD KEY `id_user` (`id_user`);

--
-- �?ndices para tabela `comment_motorized`
--
ALTER TABLE `comment_motorized`
  ADD PRIMARY KEY (`id_comment`),
  ADD KEY `id_user` (`id_user`) USING BTREE,
  ADD KEY `id_moto` (`id_motorized`) USING BTREE;

--
-- �?ndices para tabela `comment_piece`
--
ALTER TABLE `comment_piece`
  ADD PRIMARY KEY (`id_comment`),
  ADD UNIQUE KEY `id_peca` (`id_piece`,`id_user`),
  ADD KEY `id_user` (`id_user`);

--
-- �?ndices para tabela `fav_motorized`
--
ALTER TABLE `fav_motorized`
  ADD PRIMARY KEY (`id_fav_motorized`),
  ADD UNIQUE KEY `id_user` (`id_user`,`id_motorized`),
  ADD KEY `id_moto` (`id_motorized`);

--
-- �?ndices para tabela `fav_piece`
--
ALTER TABLE `fav_piece`
  ADD PRIMARY KEY (`id_fav_piece`),
  ADD UNIQUE KEY `id_user` (`id_user`,`id_piece`),
  ADD KEY `fav_peca_ibfk_2` (`id_piece`);

--
-- �?ndices para tabela `motorized`
--
ALTER TABLE `motorized`
  ADD PRIMARY KEY (`id_motorized`);

--
-- �?ndices para tabela `motorized_hire`
--
ALTER TABLE `motorized_hire`
  ADD PRIMARY KEY (`id_motorized_hire`),
  ADD KEY `id_motorized` (`id_motorized`);

--
-- �?ndices para tabela `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id_order`),
  ADD KEY `id_u` (`id_user`);

--
-- �?ndices para tabela `piece`
--
ALTER TABLE `piece`
  ADD PRIMARY KEY (`id_piece`),
  ADD UNIQUE KEY `tipo` (`type`);

--
-- �?ndices para tabela `product_order`
--
ALTER TABLE `product_order`
  ADD PRIMARY KEY (`id_product_order`),
  ADD KEY `id_order` (`id_order`),
  ADD KEY `id_motorized` (`id_motorized`);

--
-- �?ndices para tabela `rating`
--
ALTER TABLE `rating`
  ADD PRIMARY KEY (`id_rating`),
  ADD UNIQUE KEY `id_user` (`id_user`);

--
-- �?ndices para tabela `rental`
--
ALTER TABLE `rental`
  ADD PRIMARY KEY (`id_rental`),
  ADD UNIQUE KEY `id_user` (`id_user`,`id_motorized_hire`),
  ADD KEY `id_moto` (`id_motorized_hire`);

--
-- �?ndices para tabela `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_u`),
  ADD UNIQUE KEY `email` (`email`);

--
-- �?ndices para tabela `wish_list_motorized`
--
ALTER TABLE `wish_list_motorized`
  ADD PRIMARY KEY (`id_wish_list_motorized`),
  ADD KEY `id_motorized` (`id_motorized`),
  ADD KEY `id_user` (`id_user`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `cart_motorized`
--
ALTER TABLE `cart_motorized`
  MODIFY `id_cart_motorized` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT de tabela `comment_motorized`
--
ALTER TABLE `comment_motorized`
  MODIFY `id_comment` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de tabela `fav_motorized`
--
ALTER TABLE `fav_motorized`
  MODIFY `id_fav_motorized` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de tabela `fav_piece`
--
ALTER TABLE `fav_piece`
  MODIFY `id_fav_piece` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `motorized`
--
ALTER TABLE `motorized`
  MODIFY `id_motorized` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT de tabela `motorized_hire`
--
ALTER TABLE `motorized_hire`
  MODIFY `id_motorized_hire` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de tabela `order`
--
ALTER TABLE `order`
  MODIFY `id_order` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `piece`
--
ALTER TABLE `piece`
  MODIFY `id_piece` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `product_order`
--
ALTER TABLE `product_order`
  MODIFY `id_product_order` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de tabela `rating`
--
ALTER TABLE `rating`
  MODIFY `id_rating` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de tabela `rental`
--
ALTER TABLE `rental`
  MODIFY `id_rental` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `user`
--
ALTER TABLE `user`
  MODIFY `id_u` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de tabela `wish_list_motorized`
--
ALTER TABLE `wish_list_motorized`
  MODIFY `id_wish_list_motorized` int(11) NOT NULL AUTO_INCREMENT;

--
-- Restrições para despejos de tabelas
--

--
-- Limitadores para a tabela `cart_motorized`
--
ALTER TABLE `cart_motorized`
  ADD CONSTRAINT `cart_motorized_ibfk_2` FOREIGN KEY (`id_motorized`) REFERENCES `motorized` (`id_motorized`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `cart_motorized_ibfk_3` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_u`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `comment_motorized`
--
ALTER TABLE `comment_motorized`
  ADD CONSTRAINT `comment_motorized_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_u`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `comment_motorized_ibfk_2` FOREIGN KEY (`id_motorized`) REFERENCES `motorized` (`id_motorized`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `comment_piece`
--
ALTER TABLE `comment_piece`
  ADD CONSTRAINT `comment_piece_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_u`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `comment_piece_ibfk_2` FOREIGN KEY (`id_piece`) REFERENCES `piece` (`id_piece`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `fav_motorized`
--
ALTER TABLE `fav_motorized`
  ADD CONSTRAINT `fav_motorized_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_u`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fav_motorized_ibfk_2` FOREIGN KEY (`id_motorized`) REFERENCES `motorized` (`id_motorized`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `fav_piece`
--
ALTER TABLE `fav_piece`
  ADD CONSTRAINT `fav_piece_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_u`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fav_piece_ibfk_2` FOREIGN KEY (`id_piece`) REFERENCES `piece` (`id_piece`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `motorized_hire`
--
ALTER TABLE `motorized_hire`
  ADD CONSTRAINT `motorized_hire_ibfk_1` FOREIGN KEY (`id_motorized`) REFERENCES `motorized` (`id_motorized`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `order`
--
ALTER TABLE `order`
  ADD CONSTRAINT `order_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_u`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `product_order`
--
ALTER TABLE `product_order`
  ADD CONSTRAINT `product_order_ibfk_1` FOREIGN KEY (`id_order`) REFERENCES `order` (`id_order`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `product_order_ibfk_2` FOREIGN KEY (`id_motorized`) REFERENCES `motorized` (`id_motorized`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `rating`
--
ALTER TABLE `rating`
  ADD CONSTRAINT `rating_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_u`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `rental`
--
ALTER TABLE `rental`
  ADD CONSTRAINT `rental_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_u`),
  ADD CONSTRAINT `rental_ibfk_2` FOREIGN KEY (`id_motorized_hire`) REFERENCES `motorized_hire` (`id_motorized_hire`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `wish_list_motorized`
--
ALTER TABLE `wish_list_motorized`
  ADD CONSTRAINT `wish_list_motorized_ibfk_1` FOREIGN KEY (`id_motorized`) REFERENCES `motorized` (`id_motorized`) ON UPDATE CASCADE,
  ADD CONSTRAINT `wish_list_motorized_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_u`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
