<?php
  /**
   * A página onde se encontra
   */
  $header = "motorized";

  include_once('./db/db.php');
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSS & Icon  -->
    <link rel="stylesheet" href="./assets/css/global.css">
    <link rel="stylesheet" href="./assets/css/motorized/index.css">
    <link rel="stylesheet" href="./assets/css/toastr/toastr.min.css">
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="icon" href="./assets/img/logos/siteicon.ico" type="image/x-icon">

    <title>Stand Tux - Loja</title>
  </head>
  <body>
    <!-- NavBar - Header -->
    <?php 
      include './components/navbar.php'; 
      /**
       * Mostra as informações do utilizador caso a sessão estiver iniciada
       */
      if (isset($_SESSION['user_id'])) {
        $id_user = $_SESSION['user_id'];
      }      
    ?>

    <!-- Store background -->
    <div class="store-motorized">
      <div class="header-container">
          <div class="header-img-holder">
            <span>Loja - Motorizadas</span>
          </div>
      </div>

      <div class="store-container">
        <div class="panel-left menuLeft">
          <header>
            <span>Filtra por:</span>
            <i class="fa fa-bars menuFilter"></i>
          </header>

          <div class="box-filter">
            <h3>Marca</h3>

            <?php
            /**
            * Lista as marcas no menu de filtrar
            */
              function showAllBrandsInMotorized($row){
                $idGenerateUUID = 'checkMarca'.uniqid();
                echo '
                <div class="custom-control custom-checkbox checkbox-brands">
                  <input type="checkbox" class="custom-control-input" id="'.$idGenerateUUID.'">
                  <label class="custom-control-label" for="'.$idGenerateUUID.'">'.$row[0].'</label>
                </div>
                ';
              }
              $sql = "SELECT DISTINCT(UPPER(brand)) FROM motorized WHERE active=1";

              $result = $conn->query($sql);

              if ($row_cnt = $result->num_rows) {
                while ($row = mysqli_fetch_row($result)) {
                  showAllBrandsInMotorized($row);
                }
              }
            ?>
          </div>

          <hr>

          <div class="box-values">
            <h3>Potência</h3>

            <div>
              <div class="content">
                <input type="text" name="powerIn" placeholder="Potência de">
              </div>

              <div class="content">
                <input type="text" name="powerUntill" placeholder="Potência até">
              </div>
            </div>
            
          </div>

          <hr>

          <div class="box-values">
            <h3>Preço</h3>

            <div>
              <div class="content">
                <input type="text" name="priceIn" placeholder="Preço de">
              </div>

              <div class="content">
                <input type="text" name="priceUntill" placeholder="Preço até">
              </div>
            </div>
            
          </div>

          <hr>

          <div class="box-values">
            <h3>Ano</h3>

            <div>
              <div class="content">
                <input type="text" name="yearIn" placeholder="Ano de">
              </div>

              <div class="content">
                <input type="text" name="yearUntill" placeholder="Ano até">
              </div>
            </div>
            
          </div>

          <hr>
          
          <div class="btn-filter-container">
            <span id="filter-error-message" style="display:none;">Mensagem de error</span>
            <div class="filter-btn-container">
              <button>Limpar</button>
              <button>Filtrar</button>
            </div>
          </div>
        </div>
        <div class="panel-right">
          <!--<header>
            <div class="container-menu">
              <i class="fa fa-bars menuFilter hideFilter"></i>
            </div>

            <p>1 - 18 de 78 resultados</p>
          
            <div class="container">
              <div class="container-order-selected">
                <span>Ordenar por: </span>
                <div>
                  <select class="order-select">
                    <option selected>Posição</option>
                    <option value="1">Nome da motorizada</option>
                    <option value="2">Preço</option>
                  </select>
                </div>
                <span class="sort">
                  <i class="fa fa-sort"></i>
                </span>
              </div>

              <hr>

              <div class="container-order-selected">
                <span>Produtos por página: </span>
                <div>
                  <select class="order-select">
                    <option selected>18</option>
                    <option value="1">24</option>
                    <option value="2">30</option>
                  </select>
                </div>
              </div>

              <hr>

              <div class="order-showing" >
                <span>Ordenar vista: </span>
                <span><i class="fa fa-th active"></i></span>
                <span><i class="fa fa-list"></i></span>
              </div>
            </div>
          </header>-->

          <div class="search-container">
            <div class="container-menu">
              <i class="fa fa-bars menuFilter hideFilter"></i>
            </div>
            <input type="text" class="form-control" id="searchMotorized" placeholder="Pesquisar pelo nome...">
          </div>

          <div class="items-container">
            
            <?php

              $sql = "SELECT * FROM motorized";
              
              $result = $conn->query($sql);

              if ($row_cnt = $result->num_rows) {
                while ($row = mysqli_fetch_row($result)) {
                  if ($row[12] == 1) {
                    $price_promotion = round($row[4],2). ' <span>EUR</span>';
                    $fuel_decimal = substr($row[6], -2);
                    if ($fuel_decimal == "00") {
                      $row[6] = mb_substr($row[6], 0, -3);
                    }

                    if ($row[5] != 0) {
                      $price_promotion = round($row[4]*(1 - ($row[5]/100)), 2)." <span>EUR</span> | ".$row[5]."%";
                    }

                    echo '
                      <div data-id="'.$row[0].'" data-search="'.$row[1].'" class="content">
                        <div class="header">
                          <img src="'.$row[3].'" class="motorized" alt="">
                          <div>
                            <img src="./assets/img/motorized/marcas/'.strtolower($row[2]).'.png" class="logo" alt="">
                          </div>
                        </div>
                        <div class="container-info">
                          <div class="price">
                          '.$price_promotion.'
                          </div>
          
                          <h1>'.$row[1].'</h1>
          
                          <hr>
                          
                          <footer>
                            <span>'.$row[6].'L</span>
                            <span>'.$row[7].'</span>
                            <span>'.$row[8].'kw</span>
                            <span>'.$row[9].'cc</span>
                            <span>'.$row[10].'kg</span>
                          </footer>
                        </div>

                        <div class="product-redirect">
                          Ver produto
                        </div>
                      </div>
                      ';
                  }
                }
              }
            ?>
            
          </div>
        </div>
      </div>
    </div>

    <!-- Footer -->
    <?php include './components/footer.php'; ?>

    <!-- CartFixed -->
    <div class="shopping-cart-fixed">
        <div class="cart-call-left">
          <i class="fa fa-shopping-cart"></i>
        </div>
        
        <div class="cart-menu">
          <div class="cart-container">
              <?php
                $sql = "SELECT m.id_motorized, m.name, m.image 
                FROM cart_motorized as cm
                INNER JOIN motorized as m
                ON m.id_motorized = cm.id_motorized
                WHERE cm.id_user='$id_user'";

                $result = $conn->query($sql);

                if ($row_cnt = $result->num_rows) {
                  while ($row = mysqli_fetch_row($result)) {
                    echo '
                    <div class="cart-connect" data-id="'.$row[0].'">
                        <img src="'.$row[2].'" alt="'.$row[1].'">

                        <p>'.$row[1].'</p>
                    </div>
                    ';
                  }
                }
              ?>
          
            </div>
          <div class="cart-options">
            <button>Ver carrinho</button>
            <button>Limpar carrinho</button>
          </div>
        </div>
    </div>

    <div>
      <i class="fa fa-arrow-up arrowUp" aria-hidden="true"></i>
    </div>


    <?php
      /**
       * Mostra as informações do utilizador e a lista de desejos caso a sessão estiver iniciada
       */
      if (isset($_SESSION['user_id'])){
        $id_user = $_SESSION['user_id'];
        echo '
        <!-- Wish list -->
        <div class="modal fade" id="wishListMotorized" tabindex="-1" role="dialog" aria-labelledby="wishListMotorizedModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="wishListMotorizedModalLabel">Lista de desejos</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">';
                  $sql = "SELECT fm.id_fav_motorized, m.name, m.image, m.price, m.stock
                          FROM fav_motorized AS fm
                          INNER JOIN motorized AS m
                          ON m.id_motorized=fm.id_motorized
                          WHERE id_user='$id_user'
                          ";
    
                  $result = $conn->query($sql);
    
                    if ($row_cnt = $result->num_rows) {
                      echo '<table class="table table-striped">
                      <thead>
                        <tr>
                          <th scope="col">Nome</th>
                          <th scope="col">Imagem</th>
                          <th scope="col">Preço</th>
                          <th scope="col">Stock</th>
                          <th scope="col">Remover</th>
                        </tr>
                      </thead>
                      <tbody id="listWishMotorized">';
                      while ($row = mysqli_fetch_row($result)) {
                        if ($row[4]  >= 1) {
                          $row[4] = "Disponível";
                        } else {
                          $row[4] = "Esgotado";
                        }
                        echo '
                        <tr class="content" data-id="'.$row[0].'">
                          <td>'.$row[1].'</td>
                          <td><img src="'.$row[2].'" alt="'.$row[1].'"></td>
                          <td>'.$row[3].'</td>
                          <td>'.$row[4].'</td>
                          <td id="removeWishListMotorized">
                            <i class="fa fa-times"></i>
                          </td>
                        </tr>
                        ';
                      }
                      echo '</tbody>
                      </table>';
                    } else {
                      echo '<div style="text-align:center;">Nenhuma motorizada na lista de desejos</div>';
                    }
                
              echo '</div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
              </div>
            </div>
          </div>
        </div>';
    
        echo '<!-- Perfil Menu -->
        <div class="modal fade" id="perfilMenu" tabindex="-1" role="dialog" aria-labelledby="perfilMenuModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="perfilMenuModalLabel">Perfil</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="perfil-user-image">
                    <input type="file" id="file-profile-user">
    
                    <img src="'.$_SESSION['user_image'].'" id="image-profile-user" alt="'.$_SESSION['user_name'].'">
                
                    <div id="error-message-img-profile" style="display:none;">
                      <p></p>
                    </div>
                </div>
    
                <form>
                  <div class="form-row">
                    <div class="form-group col-md-6">
                      <label for="inputNamePerfil">Nome</label>
                      <input type="text" class="form-control" id="inputNamePerfil" value="'.$_SESSION['user_name'].'" placeholder="Nome e apelido">
                    </div>
                    <div class="form-group col-md-6">
                      <label for="inputEmailPerfil">Email</label>
                      <input type="email" class="form-control" id="inputEmailPerfil" value="'.$_SESSION['user_email'].'" placeholder="Email">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputAddress">Endereço</label>
                    <input type="text" class="form-control" id="inputAddress" value="'.$_SESSION['user_address'].'" placeholder="Endereço">
                  </div>
                  <div class="form-row">
                    <div class="form-group col-md-4">
                      <label for="inputCity">Cidade</label>
                      <input type="text" class="form-control" id="inputCity" value="'.$_SESSION['user_city'].'" placeholder="Cidade">
                    </div>
                    <div class="form-group col-md-4">
                      <label for="inputNumberPhone">Número telemóvel</label>
                      <input type="text" class="form-control" id="inputNumberPhone" value="'.$_SESSION['user_mobile_phone'].'" maxlength="9" placeholder="Número telemóvel">
                    </div>
                    <div class="form-group col-md-2">
                      <label for="inputNIF">NIF</label>
                      <input type="text" class="form-control" id="inputNIF" value="'.$_SESSION['user_nif'].'" maxlength="9" placeholder="Contribuinte">
                    </div>
                    <div class="form-group col-md-2">
                      <label for="inputPostalCode">Código postal</label>
                      <input type="text" class="form-control" id="inputPostalCode" value="'.$_SESSION['user_postal_cod'].'" maxlength="8" placeholder="0000-000">
                    </div>
                  </div>
                </form>
                
                <button type="button" class="btn btn-success" id="saveDataProfileUser">Guardar alterações</button>';
    
                $sql = "SELECT id_rating FROM rating WHERE id_user='$id_user'";
    
                $result = $conn->query($sql);
    
                if (!$row_cnt = $result->num_rows) {
                  echo '<hr style="height:2px;border-width:0;background-color:whitesmoke">
                  <div class="profile-rating-stars">
                    <i data-position="1" class="fa fa-star"></i>
                    <i data-position="2" class="fa fa-star"></i>
                    <i data-position="3" class="fa fa-star"></i>
                    <i data-position="4" class="fa fa-star"></i>
                    <i data-position="5" class="fa fa-star"></i>
                  </div>
                   
                   <div class="comment-home-profile-user">
                      <div class="input-group mb-3">
                        <input type="text" class="form-control" placeholder="Comentário sobre a Stand Tux...">
                        <div class="input-group-append">
                          <button class="btn btn-outline-secondary" type="button" id="sendCommentProfileUser">Enviar</button>
                        </div>
                      </div>
                      <p style="display:none;"></p>
                    </div>';
                }
    
              echo '</div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
              </div>
            </div>
          </div>
        </div>';
      }
    ?>

    <!-- Script's load -->
    <script src="./assets/js/jquery-3.5.1.min.js"></script>
    <script src="./assets/js/navbar/script.js"></script>
    <script src="./assets/js/toastr/toastr.min.js"></script>
    <script src="./assets/js/motorized/script.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>  
  </body>
</html>