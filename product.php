<?php
  if(empty($_GET['id'])) {
    echo '<script>window.location.href = "motorized";</script>';
  }
  
  include_once('./db/db.php');

  $product = [];
  $id = $_GET['id'];

  $sql = "SELECT * FROM `motorized` WHERE id_motorized='$id'";

  $result = $conn->query($sql);

  if ($row_cnt = $result->num_rows) {
    while ($row = mysqli_fetch_row($result)) {
      if ($row[5] != 0) {
        $row[4] = round($row[4]*(1 - ($row[5]/100)), 2)."€ | ".$row[5]."%";
      }


      $price_decimal = substr($row[6], -2);
      if ($price_decimal == "00") {
        $row[6] = mb_substr($row[6], 0, -3);
      }
      $product = $row;
    }
  } else {
    echo '<script>window.location.href = "motorized";</script>';
  }

  /**
  * A página onde se encontra
  */
  $header = "motorized";
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSS & Icon  -->
    <link rel="stylesheet" href="./assets/css/global.css">
    <link rel="stylesheet" href="./assets/css/product/index.css">
    <link rel="stylesheet" type="text/css" href="./assets/css/toastr/toastr.min.css">
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="icon" href="./assets/img/logos/siteicon.ico" type="image/x-icon">

    <title>Stand Tux - Loja | Produto</title>
  </head>
  <body>
    <!-- NavBar - Header -->
    <?php 
      include './components/navbar.php'; 
      /**
       * Mostra as informações do utilizador caso a sessão estiver iniciada
       */
      if (isset($_SESSION['user_id'])) {
        $id_user = $_SESSION['user_id'];
      }
    ?>

    <!-- Product background -->
    <div class="product-motorized">
      <div class="header-container">
          <div class="header-img-holder">
            <span>Loja - Motorizadas</span>
          </div>
      </div>

      <div class="product-container">
          <h3><?= $product[1] ?></h3>

          <div class="product-content">
            <img src="<?= $product[3] ?>" alt="<?= $product[1] ?>">

            <div>
                <p><?= $product[13] ?></p>

                <span><?= $product[4] ?></span>

                <div class="product-buttons" data-id="<?= $product[0] ?>">
                  <button><i class="fa fa-shopping-cart" aria-hidden="true"></i> Adicionar ao carrinho</button>
                  <button><i class="fa fa-star" aria-hidden="true"></i> Adicionar á lista de desejos</button>
                </div>
            </div>
          </div>

          <div class="product-information">
            <span><?= $product[6] ?>L</span>
            <span class="product-information-on"><?= $product[7] ?></span>
            <span><?= $product[8] ?>kw</span>
            <span class="product-information-on"><?= $product[9] ?>cc</span>
            <span><?= $product[10] ?>kg</span>
          </div>

          <div class="product-comments">
            <div class="product-comments-container">
            <?php
            /**
            * Mostra os comentários do produto
            */
            function commentProduct($row){
              echo '
              <div class="product-comments-comment">
                <p>'.$row[0].'</p>
                <hr>
                <div>
                  <span>'.$row[1].'</span>
                </div>
              </div>
              ';
            }
              $sql = "SELECT cm.comment, u.name
                      FROM comment_motorized AS cm
                      INNER JOIN user AS u
                      ON cm.id_user = u.id_u
                      WHERE cm.id_motorized='$id'";

              $result = $conn->query($sql);

              if ($row_cnt = $result->num_rows) {
                while ($row = mysqli_fetch_row($result)) {
                  commentProduct($row);
                }
              } else {
                echo '
                <div class="no-comments" style="display:flex;justify-content:center;align-items:center;margin-bottom:10px;">Sem comentários</div>
                ';
              }
            ?>
            </div>
            <?= (isset($_SESSION['user_id'])) ? '
            <footer>
                <div class="input-group" data-id="<?= $id ?>">
                  <input type="text" class="form-control" placeholder="Comentário...">
                  <div class="input-group-append">
                    <button class="btn btn-outline-secondary" type="button" id="sendComment">Enviar</button>
                  </div>
                </div>   
            </footer>' : ''
            ?>
          </div>
      </div>
    </div>

    <!-- CartFixed -->
    <div class="shopping-cart-fixed">
        <div class="cart-call-left">
          <i class="fa fa-shopping-cart"></i>
        </div>
        
        <div class="cart-menu">
          <div class="cart-container">
              <?php
              /**
               * Lista os produtos no carrinho de compras
              */
                function motorizedCart($row){
                  echo '
                  <div class="cart-connect" data-id="'.$row[0].'">
                      <img src="'.$row[2].'" alt="'.$row[1].'">

                      <p>'.$row[1].'</p>
                  </div>
                  ';
                }
                $sql = "SELECT m.id_motorized, m.name, m.image FROM cart_motorized as cm, motorized as m WHERE cm.id_user='$id_user' AND m.id_motorized = cm.id_motorized";

                $result = $conn->query($sql);

                if ($row_cnt = $result->num_rows) {
                  while ($row = mysqli_fetch_row($result)) {
                    motorizedCart($row);
                  }
                }
              ?>
          
            </div>
          <div class="cart-options">
            <button>Ver carrinho</button>
            <button>Limpar carrinho</button>
          </div>
        </div>
    </div>

    <!-- Footer -->
    <?php include './components/footer.php'; ?>

    <div>
      <i class="fa fa-arrow-up arrowUp" aria-hidden="true"></i>
    </div>

    <?php
      /**
       * Mostra as informações do utilizador e a lista de desejos caso a sessão estiver iniciada
       */
      if (isset($_SESSION['user_id'])){
        $id_user = $_SESSION['user_id'];
        echo '
        <!-- Wish list -->
        <div class="modal fade" id="wishListMotorized" tabindex="-1" role="dialog" aria-labelledby="wishListMotorizedModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="wishListMotorizedModalLabel">Lista de desejos</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">';
                  $sql = "SELECT fm.id_fav_motorized, m.name, m.image, m.price, m.stock
                          FROM fav_motorized AS fm
                          INNER JOIN motorized AS m
                          ON m.id_motorized=fm.id_motorized
                          WHERE id_user='$id_user'
                          ";
    
                  $result = $conn->query($sql);
    
                    if ($row_cnt = $result->num_rows) {
                      echo '<table class="table table-striped">
                      <thead>
                        <tr>
                          <th scope="col">Nome</th>
                          <th scope="col">Imagem</th>
                          <th scope="col">Preço</th>
                          <th scope="col">Stock</th>
                          <th scope="col">Remover</th>
                        </tr>
                      </thead>
                      <tbody id="listWishMotorized">';
                      while ($row = mysqli_fetch_row($result)) {
                        if ($row[4]  >= 1) {
                          $row[4] = "Disponível";
                        } else {
                          $row[4] = "Esgotado";
                        }
                        echo '
                        <tr class="content" data-id="'.$row[0].'">
                          <td>'.$row[1].'</td>
                          <td><img src="'.$row[2].'" alt="'.$row[1].'"></td>
                          <td>'.$row[3].'</td>
                          <td>'.$row[4].'</td>
                          <td id="removeWishListMotorized">
                            <i class="fa fa-times"></i>
                          </td>
                        </tr>
                        ';
                      }
                      echo '</tbody>
                      </table>';
                    } else {
                      echo '<div style="text-align:center;">Nenhuma motorizada na lista de desejos</div>';
                    }
                
              echo '</div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
              </div>
            </div>
          </div>
        </div>';
    
        echo '<!-- Perfil Menu -->
        <div class="modal fade" id="perfilMenu" tabindex="-1" role="dialog" aria-labelledby="perfilMenuModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="perfilMenuModalLabel">Perfil</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="perfil-user-image">
                    <input type="file" id="file-profile-user">
    
                    <img src="'.$_SESSION['user_image'].'" id="image-profile-user" alt="'.$_SESSION['user_name'].'">
                
                    <div id="error-message-img-profile" style="display:none;">
                      <p></p>
                    </div>
                </div>
    
                <form>
                  <div class="form-row">
                    <div class="form-group col-md-6">
                      <label for="inputNamePerfil">Nome</label>
                      <input type="text" class="form-control" id="inputNamePerfil" value="'.$_SESSION['user_name'].'" placeholder="Nome e apelido">
                    </div>
                    <div class="form-group col-md-6">
                      <label for="inputEmailPerfil">Email</label>
                      <input type="email" class="form-control" id="inputEmailPerfil" value="'.$_SESSION['user_email'].'" placeholder="Email">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputAddress">Endereço</label>
                    <input type="text" class="form-control" id="inputAddress" value="'.$_SESSION['user_address'].'" placeholder="Endereço">
                  </div>
                  <div class="form-row">
                    <div class="form-group col-md-4">
                      <label for="inputCity">Cidade</label>
                      <input type="text" class="form-control" id="inputCity" value="'.$_SESSION['user_city'].'" placeholder="Cidade">
                    </div>
                    <div class="form-group col-md-4">
                      <label for="inputNumberPhone">Número telemóvel</label>
                      <input type="text" class="form-control" id="inputNumberPhone" value="'.$_SESSION['user_mobile_phone'].'" maxlength="9" placeholder="Número telemóvel">
                    </div>
                    <div class="form-group col-md-2">
                      <label for="inputNIF">NIF</label>
                      <input type="text" class="form-control" id="inputNIF" value="'.$_SESSION['user_nif'].'" maxlength="9" placeholder="Contribuinte">
                    </div>
                    <div class="form-group col-md-2">
                      <label for="inputPostalCode">Código postal</label>
                      <input type="text" class="form-control" id="inputPostalCode" value="'.$_SESSION['user_postal_cod'].'" maxlength="8" placeholder="0000-000">
                    </div>
                  </div>
                </form>
                
                <button type="button" class="btn btn-success" id="saveDataProfileUser">Guardar alterações</button>';
    
                $sql = "SELECT id_rating FROM rating WHERE id_user='$id_user'";
    
                $result = $conn->query($sql);
    
                if (!$row_cnt = $result->num_rows) {
                  echo '<hr style="height:2px;border-width:0;background-color:whitesmoke">
                  <div class="profile-rating-stars">
                    <i data-position="1" class="fa fa-star"></i>
                    <i data-position="2" class="fa fa-star"></i>
                    <i data-position="3" class="fa fa-star"></i>
                    <i data-position="4" class="fa fa-star"></i>
                    <i data-position="5" class="fa fa-star"></i>
                  </div>
                   
                   <div class="comment-home-profile-user">
                      <div class="input-group mb-3">
                        <input type="text" class="form-control" placeholder="Comentário sobre a Stand Tux...">
                        <div class="input-group-append">
                          <button class="btn btn-outline-secondary" type="button" id="sendCommentProfileUser">Enviar</button>
                        </div>
                      </div>
                      <p style="display:none;"></p>
                    </div>';
                }
    
              echo '</div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
              </div>
            </div>
          </div>
        </div>';
      }
    ?>

    <!-- Script's load -->
    <script src="./assets/js/jquery-3.5.1.min.js"></script>
    <script src="./assets/js/toastr/toastr.min.js"></script>
    <script src="./assets/js/navbar/script.js"></script>
    <script src="./assets/js/product/script.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>  
  </body>
</html>