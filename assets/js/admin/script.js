function notificationTopRight() {
	//Estilo das Notifications
	toastr.options = {
		"closeButton": false,
		"debug": true,
		"newestOnTop": true,
		"progressBar": true,
		"positionClass": "toast-top-right",
		"preventDuplicates": true,
		"onclick": null,
		"showDuration": "300",
		"hideDuration": "1000",
		"timeOut": "5000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
	}
}

$(document).ready(function(){
  $("#tabs").tabs();

  $(document).on('click', '#tabs ul li:nth-child(1)', function () { 
    $('button[data-target="#addMotorized"]').css('display', 'block')
    $('button[data-target="#addBrand"]').css('display', 'none')
  })

  $(document).on('click', '#tabs ul li:nth-child(2)', function () { 
    $('button[data-target="#addMotorized"]').css('display', 'none')
    $('button[data-target="#addBrand"]').css('display', 'none')
  })

  $(document).on('click', '#tabs ul li:nth-child(3)', function () { 
    $('button[data-target="#addMotorized"]').css('display', 'none')
    $('button[data-target="#addBrand"]').css('display', 'block')
  })

  $(document).on('click', '#createMotorized', function(){
    const name = $('#inputName').val()
    const brand = $('#inputBrand').val()
    const price = $('#inputPrice').val()
    const promotion = $('#inputPromotion').val()
    const fuelcapacity = $('#inputFuelCapacity').val()
    const yearmanufacturing = $('#inputYearManufacturing').val()
    const power = $('#inputPower').val()
    const cylinder = $('#inputCylinder').val()
    const weight = $('#inputWeight').val()
    const stock = $('#inputStock').val()
    const description = $('#textareaDescription').val()
    const checked = $('#active').is(':checked') ? 1 : 0

    $.ajax({
      url: './ajax/createmotorizedadminpanel',
      method: 'POST',
      dataType: 'JSON',
      data: {
        update: 1,
        name,
        brand,
        price,
        promotion,
        fuelcapacity,
        yearmanufacturing,
        power,
        cylinder,
        weight,
        stock,
        description,
        checked
      },
      success: function(response){
        if (response[0] === 1){
          const checkedConfirm = checked ? 'Ativo' : 'Desativado'

          $('#motorized_tbody').append(''+
          '<tr data-id="'+response[1]+'">'+
              '<th>'+
                '<a href="/product?id='+response[1]+'">'+
                  name+
                '</a>'+
              '</th>'+
              '<td>'+brand+'</td>'+
              '<td>'+price+'€</td>'+
              '<td>'+promotion+'%</td>'+
              '<td>'+fuelcapacity+'</td>'+
              '<td>'+yearmanufacturing+'</td>'+
              '<td>'+power+'</td>'+
              '<td>'+cylinder+'</td>'+
              '<td>'+weight+'</td>'+
              '<td>'+stock+'</td>'+
              '<td>'+checkedConfirm+'</td>'+
              '<td>'+
                '<div class="paineladmin-edit">'+
                    '<i class="fa fa-eye" title="'+description+'"></i>'+
                    '<i class="fa fa-edit" data-toggle="modal" data-target="#editMotorized"></i>'+
                    '<i class="fa fa-trash"></i>'+
                '</div>'+
              '</td>'+
          '</tr>')

          $('#createMotorizedCancel').click()

          notificationTopRight();
          toastr["success"]("Adicionanda nova motorizada!", "Motorizada criada")
        }
      }
    })
  });

  $(document).on('click', '.paineladmin .paineladmin-edit .fa-trash', function(){
    const motorized_id = $(this).parents('tr').attr('data-id')

    const confirmCheck = confirm('Tem certeza?')

    if (confirmCheck) {
      $.ajax({
        url: './ajax/deletemotorizedadminpanel',
        method: 'POST',
        dataType: 'TEXT',
        data: {
          update: 1,
          motorized_id
        },
        success: function(){
          $('#motorized_tbody tr[data-id="'+motorized_id+'"]').remove()
  
          notificationTopRight();
          toastr["success"]("Motorizada removida com sucesso!", "Motorizada removida")
        }
      })
    }
  })

  $(document).on('click', '.paineladmin .paineladmin-edit .fa-edit', function(){
    const motorized_id = $(this).parents('tr').attr('data-id')

    const name = $('#motorized_tbody tr[data-id="'+motorized_id+'"] th:first').text()
    const brand = $('#motorized_tbody tr[data-id="'+motorized_id+'"] td:nth-child(2)').text()
    const price = $('#motorized_tbody tr[data-id="'+motorized_id+'"] td:nth-child(3)').text().slice(0, -1)
    const promotion = $('#motorized_tbody tr[data-id="'+motorized_id+'"] td:nth-child(4)').text().slice(0, -1)
    const fuelcapacity = $('#motorized_tbody tr[data-id="'+motorized_id+'"] td:nth-child(5)').text()
    const yearmanufacturing = $('#motorized_tbody tr[data-id="'+motorized_id+'"] td:nth-child(6)').text()
    const power = $('#motorized_tbody tr[data-id="'+motorized_id+'"] td:nth-child(7)').text()
    const cylinder = $('#motorized_tbody tr[data-id="'+motorized_id+'"] td:nth-child(8)').text()
    const weight = $('#motorized_tbody tr[data-id="'+motorized_id+'"] td:nth-child(9)').text()
    const stock = $('#motorized_tbody tr[data-id="'+motorized_id+'"] td:nth-child(10)').text()
    const checked = $('#motorized_tbody tr[data-id="'+motorized_id+'"] td:nth-child(11)').text()
    const description = $('#motorized_tbody tr[data-id="'+motorized_id+'"] td:nth-child(12) .fa-eye').attr('title')

    $('#inputEditName').val(name)
    $('#inputEditBrand').val(brand)
    $('#inputEditPrice').val(price)
    $('#inputEditPromotion').val(promotion)
    $('#inputEditFuelCapacity').val(fuelcapacity)
    $('#inputEditYearManufacturing').val(yearmanufacturing)
    $('#inputEditPower').val(power)
    $('#inputEditCylinder').val(cylinder)
    $('#inputEditWeight').val(weight)
    $('#inputEditStock').val(stock)
    $('#textareaEditDescription').val(description)
    if (checked === 'Ativo') {
      $('#activeEdit').attr('checked', true)
    }

    $('#editMotorizedBtn').attr('data-id', motorized_id)
  })

  $(document).on('click', '#editMotorizedBtn', function () {
    const motorized_id = $('#editMotorizedBtn').attr('data-id')

    const name = $('#inputEditName').val()
    const brand = $('#inputEditBrand').val()
    const price = $('#inputEditPrice').val()
    const promotion = $('#inputEditPromotion').val()
    const fuelcapacity = $('#inputEditFuelCapacity').val()
    const yearmanufacturing = $('#inputEditYearManufacturing').val()
    const power = $('#inputEditPower').val()
    const cylinder = $('#inputEditCylinder').val()
    const weight = $('#inputEditWeight').val()
    const stock = $('#inputEditStock').val()
    const description = $('#textareaEditDescription').val()
    let checked = $('#activeEdit').is(':checked') ? 1 : 0

    $.ajax({
      url: './ajax/editmotorizedadminpanel',
      method: 'POST',
      dataType: 'TEXT',
      data: {
        update: 1,
        motorized_id,
        name,
        brand,
        price,
        promotion,
        fuelcapacity,
        yearmanufacturing,
        power,
        cylinder,
        weight,
        stock,
        description,
        checked
      },
      success: function(){
        if (checked === 1) {
          checked = "Ativo"
        } else {
          checked = "Desativado"
        }
    
        $('#motorized_tbody tr[data-id="'+motorized_id+'"] th:first a').text(name)
        $('#motorized_tbody tr[data-id="'+motorized_id+'"] td:nth-child(2)').text(brand)
        $('#motorized_tbody tr[data-id="'+motorized_id+'"] td:nth-child(3)').text(price+'€')
        $('#motorized_tbody tr[data-id="'+motorized_id+'"] td:nth-child(4)').text(promotion+'%')
        $('#motorized_tbody tr[data-id="'+motorized_id+'"] td:nth-child(5)').text(fuelcapacity)
        $('#motorized_tbody tr[data-id="'+motorized_id+'"] td:nth-child(6)').text(yearmanufacturing)
        $('#motorized_tbody tr[data-id="'+motorized_id+'"] td:nth-child(7)').text(power)
        $('#motorized_tbody tr[data-id="'+motorized_id+'"] td:nth-child(8)').text(cylinder)
        $('#motorized_tbody tr[data-id="'+motorized_id+'"] td:nth-child(9)').text(weight)
        $('#motorized_tbody tr[data-id="'+motorized_id+'"] td:nth-child(10)').text(stock)
        $('#motorized_tbody tr[data-id="'+motorized_id+'"] td:nth-child(11)').text(checked)
        $('#motorized_tbody tr[data-id="'+motorized_id+'"] td:nth-child(12) .fa-eye').attr('title', description)
        
        $('#editMotorizedCancel').click()
      }
    })
  })

  $(document).on('click', '#aproveCommentsInHome', function(){
    let check = ($(this).parent().find('td:nth-child(4)').text() === 'Sim') ? true : false
    const rating_id = $(this).parent().attr('data-id')


    var counter = 0
    $('#rating_tbody .content').filter(function(){
      if ($(this).find('td:nth-child(4)').text() === 'Sim') {
        counter = counter + 1
      }
    })

    if (!check && counter >= 3) {
      return;
    }

    const varthis = $(this)

    const confirmCheck = confirm("Tem certeza?")

    if (!check) {
      check = 1
    } else {
      check = 0
    }

    if (confirmCheck) {
      $.ajax({
        url: './ajax/aprovecommentsinhome',
        method: 'POST',
        dataType: 'TEXT',
        data: {
          update: 1,
          check,
          rating_id
        },
        success: function(){
          if (check == 0) {
            notificationTopRight();
            toastr["success"]("Comentário desativado!", "Comentário(home)")
            varthis.parent().find('td:nth-child(4)').text('Não')
            varthis.html('<i class="fa fa-check"></i>')
          } else {
            notificationTopRight();
            toastr["success"]("Comentário ativado!", "Comentário(home)")
            varthis.parent().find('td:nth-child(4)').text('Sim')
            varthis.html('<i class="fa fa-times"></i>')
          }
        }
      })
    }
  })

  $("#addBrandFile").on("change", function(event) {
    const image = event.target.files[0]

    var fd = new FormData(); 
    fd.append("file", image);

    const typeValidate = ['png']
    const type = image.type.split("/")[1]

    if (typeValidate.includes(type)) {
      const fileName = $(this).val().split("\\").pop();
      $(this).siblings(".custom-file-label").addClass("selected").html(fileName);

      $('#error-message-img-brand').css('display', 'none')

      $.ajax({
        type: 'POST',
        url: 'ajax/uploadimagebrand',
        dataType: 'TEXT',
        data: fd,
        contentType: false,
        processData: false,
        success: function () {
          notificationTopRight();
          toastr["success"](`A marca ${fileName} foi adicionanda com sucesso!`, "Marcas(imagem)")
          $('#addBrandCancel').click()
        }
      });
    } else {
      $('#error-message-img-brand').css({
        display: 'block',
        color: '#ce4043'
      }).find('p').text('Ficheiro do tipo '+type+' não é permitido')
    }
  });

  $("#editMotorizedFile").on("change", function(event) {
    const image = event.target.files[0]
    const motorized_id = $('#editMotorizedBtn').attr('data-id')

    var fd = new FormData(); 
    fd.append("file", image);
    fd.append("motorized_id", motorized_id);

    const typeValidate = ['png']
    const type = image.type.split("/")[1]

    if (typeValidate.includes(type)) {
      const fileName = $(this).val().split("\\").pop();
      $(this).siblings(".custom-file-label").addClass("selected").html(fileName);

      $('#error-message-img-motorized').css('display', 'none')

      $.ajax({
        type: 'POST',
        url: 'ajax/uploadimagemotorized',
        dataType: 'TEXT',
        data: fd,
        contentType: false,
        processData: false,
        success: function () {
          notificationTopRight();
          toastr["success"]('Imagem alterada com sucesso!', "Imagem motorizada alterada")
          $('#editMotorizedCancel').click()
        }
      });
    } else {
      $('#error-message-img-motorized').css({
        display: 'block',
        color: '#ce4043'
      }).find('p').text('Ficheiro do tipo '+type+' não é permitido')
    }
  });

  $(document).on('click', '#brand_tbody .content td:nth-child(3)', function () {
    const fileName = $(this).parents('tr').find('td:nth-child(1)').text().toLowerCase() + '.png'
    const varthis = $(this)

    $.ajax({
      type: 'POST',
      url: 'ajax/deleteimagebrand',
      dataType: 'TEXT',
      data: {
        update: 1,
        fileName
      },
      success: function () {
        varthis.parents('tr').remove()
        notificationTopRight();
        toastr["success"](`A marca ${fileName.split('.')[0]} foi apagada com sucesso!`, "Marcas(imagem)")
      }
    });
  })
});