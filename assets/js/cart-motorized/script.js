$(document).ready(function(){
  $(document).on('click', '.shoppingCart-right .fa-trash', function () {
    const motorized_id = $(this).parents('.shoppingCart-content').attr('data-id')

    $.ajax({
      url: './ajax/removemotorizedincart',
      method: 'POST',
      dataType: 'TEXT',
      data: {
        update: 1,
        motorized_id
      },
      success: function(){
        window.location.href = "/cart-motorized"
      }
    });
  })
})