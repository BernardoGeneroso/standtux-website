function notificationTopRight() {
	//Estilo das Notifications
	toastr.options = {
		"closeButton": false,
		"debug": true,
		"newestOnTop": true,
		"progressBar": true,
		"positionClass": "toast-top-right",
		"preventDuplicates": true,
		"onclick": null,
		"showDuration": "300",
		"hideDuration": "1000",
		"timeOut": "5000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
	}
}

$(document).ready(function(){
  $(document).on('click', '#fa-power-off', function(event){
    event.preventDefault()

    $.ajax({
      url: './ajax/authdestroy',
      method: 'POST',
      dataType: 'TEXT',
      data: {
        update: 1
      },
      success: function(response){
        if (response == 1) {
          window.location.href = "/"
        }
      }
    });
  });

  //Cart
  $('.shopping-cart-fixed').mouseover(function(){
    $(this).find('.fa-shopping-cart').css('opacity', 0.8)
  }).mouseout(function(){
    $(this).find('.fa-shopping-cart').css('opacity', 1)
  }).click(function(){
    if ($(this).css('height') === "320px") {
      $(this).animate({height: '85px', width: '72px'})
      $(this).find('.cart-menu').fadeOut(400)
      $(this).find('.cart-call-left').css('margin-right', "auto")
      $(this).find('.cart-call-left i').removeClass('fa-times').addClass('fa-shopping-cart')
    } else {
      var thisClass = $(this)
      $.ajax({
        url: './ajax/idusercheck',
        method: 'POST',
        dataType: 'TEXT',
        data: {
          update: 1
        },
        success: function(response){
          if (response == 1) {
            $.ajax({
              url: './ajax/productcart',
              method: 'POST',
              dataType: 'JSON',
              data: {
                update: 1
              },
              success: function(cart){
                var carts = "";
                for (var i = 0; i < cart.length; i++) {
                  carts += '<div class="cart-connect" data-id="'+cart[i][0]+'"><img src="'+cart[i][2]+'" alt="'+cart[i][1]+'"><p>'+cart[i][1]+'</p></div>'
                }

                $('.shopping-cart-fixed .cart-menu .cart-container').html(carts)
              }
            })
            thisClass.animate({height: '320px', width: "360px"}).css('padding', '24px 0px 24px 24px')
            setTimeout(function(){ $('.shopping-cart-fixed').find('.cart-menu').fadeIn().css('display', 'flex'); }, 400);
            thisClass.find('.cart-call-left').css('margin-right', 18)
            thisClass.find('.cart-call-left i').removeClass('fa-shopping-cart').addClass('fa-times')
          } else {
            window.location.href = "/signin"
          }
        }
      })
    }
  })

  $('.shopping-cart-fixed .cart-menu .cart-options button:nth-child(1)').click(function(){
    window.location.href = "/cart-motorized"
  })

  $('.shopping-cart-fixed .cart-menu .cart-options button:nth-child(2)').click(function(){
    const varthis = $(this)

    $.ajax({
      url: './ajax/cleancart',
      method: 'POST',
      dataType: 'TEXT',
      data: {
        update: 1
      }, success: function(){
        varthis.parents('.cart-menu .cart-container').remove()
      }
    });
  })

  $(document).on('click', '#removeWishListMotorized', function(){
    const wish_list_id = $(this).parent().attr('data-id')

    const varthis = $(this)

    const confirmCheck = confirm('Tem certeza?')

    if (confirmCheck) {
      $.ajax({
        url: './ajax/deleteitemwishlist',
        method: 'POST',
        dataType: 'TEXT',
        data: {
          update: 1,
          wish_list_id
        },
        success: function(){
          varthis.parents('.content').remove()
        }
      });
    }
  })

  $(document).on('click', '#image-profile-user', function(){
    $('#file-profile-user').click()
  })

  $(document).on('change', '#file-profile-user', function(event){
    const image = event.target.files[0]

    var fd = new FormData(); 
    fd.append("file", image);

    const typeValidate = ['png', 'jpg', 'jpeg']
    const type = image.type.split("/")[1]

    if (typeValidate.includes(type)) {
      $('#error-message-img-profile').css('display', 'none')

      $.ajax({
        type: 'POST',
        url: 'ajax/uploadimageprofile',
        dataType: 'JSON',
        data: fd,
        contentType: false,
        processData: false,
        success: function(response){
          $('#image-profile-user').attr('src', response)
          $('#image-user').attr('src', response)
        }
      });
    } else {
      $('#error-message-img-profile').css({
        display: 'block',
        color: '#ce4043'
      }).find('p').text('Ficheiro do tipo '+type+' não é permitido')
    }
  })

  $(document).on('click', '#saveDataProfileUser', function(){
    const name = $('#inputNamePerfil').val()
    const email = $('#inputEmailPerfil').val()
    const address = $('#inputAddress').val()
    const city = $('#inputCity').val()
    const mobile_phone = $('#inputNumberPhone').val()
    const nif = $('#inputNIF').val()
    const postal_code = $('#inputPostalCode').val()

    $.ajax({
      type: 'POST',
      url: 'ajax/userprofilesave',
      dataType: 'TEXT',
      data: {
        update: 1,
        name,
        email,
        address,
        city,
        mobile_phone,
        nif,
        postal_code
      },
      success: function(){
        $('#image-user').parent().find('div span:nth-child(2)').text(name)

        notificationTopRight();
        toastr["success"]("Os seus dados foram guardados com sucesso!", "Dados guardados")
      }
    });
  })

  $(document).on('mouseenter', '.profile-rating-stars .fa-star', function(){
    const position = $(this).attr('data-position')

    for (var i = 0; i < position; i++) {
      $('.profile-rating-stars .fa-star:nth-child('+(i+1)+')').css('color', '#ce4043')
    }

    if (position != 5) {
      for (var i = 5; i > position; i--) {
        $('.profile-rating-stars .fa-star:nth-child('+(i)+')').css('color', '')
      }
    }
  })

  var leaveRating = false

  $(document).on('mouseleave', '.profile-rating-stars .fa-star', function(){
    if (!leaveRating) {
      for (var i = 0; i < 5; i++) {
        $('.profile-rating-stars .fa-star:nth-child('+(i+1)+')').css('color', '')
      }
    }
  })

  $(document).on('click', '.profile-rating-stars .fa-star', function(){
    const position = $(this).attr('data-position')

    for (var i = 0; i < position; i++) {
      $('.profile-rating-stars .fa-star:nth-child('+(i+1)+')').css('color', '#ce4043')
    }

    if (position != 5) {
      for (var i = 5; i > position; i--) {
        $('.profile-rating-stars .fa-star:nth-child('+(i)+')').css('color', '')
      }
    }

    leaveRating = true;
  })

  $(document).on('click', '#sendCommentProfileUser', function(){
    if ($('.profile-rating-stars i:nth-child(1)').css('color') != 'rgb(206, 64, 67)'){
      $(this).parents('.comment-home-profile-user').find('p').css({display: 'block', color: '#ce4043', textAlign: 'center'}).text('Precisa de escolher uma classificação')
      return;
    }

    var rating = 0
    $('.profile-rating-stars .fa-star').filter(function(){
      if ($(this).css('color') == 'rgb(206, 64, 67)'){
        rating = rating + 1
      }
    })

    const description = $(this).parents('.input-group').find('input').val()
    const varthis = $(this)

    if (description !== ''){
      $.ajax({
        type: 'POST',
        url: 'ajax/createcommenthome',
        dataType: 'TEXT',
        data: {
          update: 1,
          rating,
          description
        },
        success: function(){
          varthis.parents('.modal-body').find('hr').remove()
          varthis.parents('.modal-body').find('.profile-rating-stars').remove()
          varthis.parents('.modal-body').find('.comment-home-profile-user').remove()
  
          notificationTopRight();
          toastr["success"]("Comentário guardado, agradeçemos a sua opinião...", "Commentário Stand Tux")
        }
      });
    } else {
      $(this).parents('.comment-home-profile-user').find('p').css({display: 'block', color: '#ce4043', textAlign: 'center'}).text('O campo do comentário está vazio...')
      return;
    }
  });
})