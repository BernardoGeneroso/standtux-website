$('.arrowUp').hide()
$('.hideFilter').hide()

$(document).ready(function(){
  //Seta scroll
  $(window).on( 'scroll', function(){
    if ($(this).scrollTop() >= 20) {
      $('.arrowUp').fadeIn("fast")
    } else {
      $('.arrowUp').fadeOut("fast")
    }
  });

  $('.arrowUp').click(function(){
    $("html, body").animate({ scrollTop: 0 }, 600);
      return false;
  });
  //

  $('.store-motorized .store-container .panel-right .items-container .content').mouseover(function(){
    $(this).find(".product-redirect").addClass('active-product-redirect')
  })

  $('.store-motorized .store-container .panel-right .items-container .content').mouseout(function(){
    $(this).find(".product-redirect").removeClass('active-product-redirect')
  })
  //

  $('.menuFilter').click(function(){
    if ($(this).parent().hasClass("container-menu")) {
      $('.search-container input').css('margin-left', '0')
      $('.hideFilter').fadeOut()
      $('.menuLeft').fadeIn()
    } else {
      $('.search-container input').css('margin-left', '10px')
      $('.hideFilter').fadeIn()
      $('.menuLeft').fadeOut()
    }
  })
  //

  $('.store-motorized .store-container .panel-right .items-container .content').click(function(){
    window.location.href='product?id='+$(this).attr('data-id');
  })

  $(document).on('keyup', '#searchMotorized', function () { 
    const search = $(this).val().toLowerCase()

    $(".items-container .content").filter(function() {
      $(this).toggle($(this).attr('data-search').toLowerCase().indexOf(search) > -1)
    });
  })

  const filterErrorMessage = (option, message = "") => {
    if (option === 1) {
      $('#filter-error-message').css({
        display: 'block',
        color: '#ce4043'
      }).text(message)
    } else {
      $('#filter-error-message').css({
        display: 'none',
        color: ''
      })
    }
  }

  $(document).on('click', '.btn-filter-container div button:nth-child(2)', function () {
    const checkboxChecked = []
    const power = [parseFloat($('input[name="powerIn"]').val()),parseFloat($('input[name="powerUntill"]').val())]
    const price = [parseFloat($('input[name="priceIn"]').val()),parseFloat($('input[name="priceUntill"]').val())]
    const year = [parseFloat($('input[name="yearIn"]').val()),parseFloat($('input[name="yearUntill"]').val())]

    $('.checkbox-brands').each(function (i, index) {
      const path = $('.checkbox-brands:nth-child('+(i+1)+')')
      if (path.find('input').is(":checked")) {
        checkboxChecked.push(path.find('label').text().toLowerCase())
      }
    });

    if (power[0] > power[1]) {
      filterErrorMessage(1, "Erro com os valores das potências")
      return;
    } else if (price[0] > price[1]) {
      filterErrorMessage(1, "Erro com os valores do preço")
      return;
    } else if (year[0] > year[1]) {
      filterErrorMessage(1, "Erro com os valores do ano")
      return;
    } else if (checkboxChecked.length === 0
            && Number.isNaN(power[0]) 
            && Number.isNaN(power[1]) 
            && Number.isNaN(price[0]) 
            && Number.isNaN(price[1])
            && Number.isNaN(year[0])
            && Number.isNaN(year[1])) {
      let check = false
      $(".items-container .content").filter(function(){
        if ($(this).css('display') === 'none'){
          check = true
        }
        $(this).css('display', 'block')
      })
      if (!check) {
        filterErrorMessage(1, "Nenhum filtro adicionado")
      }
      return;
    } else {
      filterErrorMessage(0)
    }

    if (checkboxChecked.length === 0) {
      $(".items-container .content").filter(function () {
        $(this).css('display', 'block')
          let verificatedParams = []

          if (!Number.isNaN(power[0]) || !Number.isNaN(power[1])) {
            const powerValue = parseFloat($(this).find('footer span:nth-child(3)').text().slice(0, -2))
            if (power[0] <= powerValue && power[1] >= powerValue){
              verificatedParams.push(true)
            } else {
              verificatedParams.push(false)
            }
          }

          if (!Number.isNaN(price[0]) || !Number.isNaN(price[1])) {
            const priceValue = parseFloat($(this).find('.price').text().trim().split(" ")[0])
            if (price[0] <= priceValue && price[1] >= priceValue){
              verificatedParams.push(true)
            } else {
              verificatedParams.push(false)
            }
          }

          if (!Number.isNaN(year[0]) || !Number.isNaN(year[1])) {
            const yearValue = parseFloat($(this).find('footer span:nth-child(2)').text())
            if (year[0] <= yearValue && year[1] >= yearValue){
              verificatedParams.push(true)
            } else {
              verificatedParams.push(false)
            }
          }

          if (verificatedParams.includes(false)) {
            $(this).css('display', 'none')
          }
      })
    } else {
      $(".items-container .content").filter(function () {
        const nameBrand = $(this).find('h1').text().toLowerCase().split(" ")[0]
        if (checkboxChecked.includes(nameBrand)) {
          $(this).css('display', 'block')
          let verificatedParams = []

          if (!Number.isNaN(power[0]) || !Number.isNaN(power[1])) {
            const powerValue = parseFloat($(this).find('footer span:nth-child(3)').text().slice(0, -2))
            if (power[0] <= powerValue && power[1] >= powerValue){
              verificatedParams.push(true)
            } else {
              verificatedParams.push(false)
            }
          }

          if (!Number.isNaN(price[0]) || !Number.isNaN(price[1])) {
            const priceValue = parseFloat($(this).find('.price').text().trim().split(" ")[0])
            if (price[0] <= priceValue && price[1] >= priceValue){
              verificatedParams.push(true)
            } else {
              verificatedParams.push(false)
            }
          }

          if (!Number.isNaN(year[0]) || !Number.isNaN(year[1])) {
            const yearValue = parseFloat($(this).find('footer span:nth-child(2)').text())
            if (year[0] <= yearValue && year[1] >= yearValue){
              verificatedParams.push(true)
            } else {
              verificatedParams.push(false)
            }
          }

          if (verificatedParams.includes(false)) {
            $(this).css('display', 'none')
          }
        } else {
          $(this).css('display', 'none')
        }
      })
    }
  })

  $(document).on('click', '.btn-filter-container div button:nth-child(1)', function () {
    $('.checkbox-brands').each(function (i, index) {
      const path = $('.checkbox-brands:nth-child('+(i+1)+')')
      if (path.find('input').is(":checked")) {
        path.find('input').prop('checked', false)
      }
    });

    $('input[name="powerIn"]').val('')
    $('input[name="powerUntill"]').val('')
    $('input[name="priceIn"]').val('')
    $('input[name="priceUntill"]').val('')
    $('input[name="yearIn"]').val('')
    $('input[name="yearUntill"]').val('')

    $(".items-container .content").filter(function(){
      $(this).css('display', 'block')
    })
  })
});