function notificationTopRight() {
	//Estilo das Notifications
	toastr.options = {
		"closeButton": false,
		"debug": true,
		"newestOnTop": true,
		"progressBar": true,
		"positionClass": "toast-top-right",
		"preventDuplicates": true,
		"onclick": null,
		"showDuration": "300",
		"hideDuration": "1000",
		"timeOut": "5000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
	}
}

$('.arrowUp').hide()

$(document).ready(function(){
  //Seta scroll
  $(window).on( 'scroll', function(){
    if ($(this).scrollTop() >= 20) {
      $('.arrowUp').fadeIn("fast")
    } else {
      $('.arrowUp').fadeOut("fast")
    }
  });

  $('.arrowUp').click(function(){
    $("html, body").animate({ scrollTop: 0 }, 600);
      return false;
  });
  //

  $('.product-motorized .product-container .product-content .product-buttons button:nth-child(1)').click(function(){
    $.ajax({
      url: './ajax/idusercheck',
      method: 'POST',
      dataType: 'TEXT',
      data: {
        update: 1
      },
      success: function(response){
        if (response == 0) {
          window.location.href = "/signin"
        }
      }
    })

    const nameOfMotorized = $('.product-container h3').text()
    const productID = $(this).parent().attr('data-id')
    const srcIMG = $(this).parents('.product-content').find('img').attr('src')

    $.ajax({
      url: './ajax/addproductcart',
      method: 'POST',
      dataType: 'TEXT',
      data: {
        update: 1,
        productID
      },
      success: function(response){
        if (response == 1){
          notificationTopRight();
          toastr["error"]("A motorizada "+nameOfMotorized+" já se encontra no seu carrinho", "Erro ao adicionar no carrinho")
        } else if(response == 2) {
          $('.cart-container').append(''+
              '<div class="cart-connect" data-id="'+productID+'">'+
                  '<img src="'+srcIMG+'" alt="'+nameOfMotorized+'">'+

                  '<p>'+nameOfMotorized+'</p>'+
              '</div>')

          notificationTopRight();
          toastr["success"]("A motorizada "+nameOfMotorized+" foi adicionando ao seu carrinho", "Novo produto")
        }
      }
    })
  })

  $('.product-motorized .product-container .product-content .product-buttons button:nth-child(2)').click(function(){
    $.ajax({
      url: './ajax/idusercheck',
      method: 'POST',
      dataType: 'TEXT',
      data: {
        update: 1
      },
      success: function(response){
        if (response == 0) {
          window.location.href = "/signin"
        }
      }
    })

    const nameOfMotorized = $('.product-container h3').text()
    
    $.ajax({
      url: './ajax/addproductwish',
      method: 'POST',
      dataType: 'TEXT',
      data: {
        update: 1,
        productID: $(this).parent().attr('data-id')
      },
      success: function(response){
        if (response == 1){
          notificationTopRight();
          toastr["error"]("A motorizada "+nameOfMotorized+" foi removida da lista de desejos", "Informação lista de desejos")
        } else if(response == 2) {
          notificationTopRight();
          toastr["success"]("A motorizada "+nameOfMotorized+" foi adicionanda á lista de desejos", "Informação lista de desejos")
        }
      }
    })
  })

  $(document).on('click', '#sendComment', function(){
    const comment = $(this).parents('.input-group').find('input').val()

    if (comment !== "") {
      const motorized_id = $(this).parents('.input-group').attr('data-id')

      if($('.no-comments').length > 0) {
        $('.no-comments').remove()
      }

      $.ajax({
        url: './ajax/addcommentmotorized',
        method: 'POST',
        dataType: 'TEXT',
        data: {
          update: 1,
          comment,
          motorized_id
        },
        success: function(user_name){
          notificationTopRight();
          toastr["success"]("O comentário foi adicionado com sucesso", "Informação sobre os comentários")

          $('.product-comments-container').append(''+
            '<div class="product-comments-comment">'+
              '<p>'+comment+'</p>'+
              '<hr>'+
              '<div>'+
                '<span>'+user_name+'</span>'+
              '</div>'+
            '</div>'
          )
        }
      })
    }
  })
});