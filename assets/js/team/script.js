$('.arrowUp').hide()

$(document).ready(function(){
  //Seta scroll
  $(window).on( 'scroll', function(){
    if ($(this).scrollTop() >= 20) {
      $('.arrowUp').fadeIn("fast")
    } else {
      $('.arrowUp').fadeOut("fast")
    }
  });

  $('.arrowUp').click(function(){
    $("html, body").animate({ scrollTop: 0 }, 600);
      return false;
  });
});