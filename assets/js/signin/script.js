function validateEmail(email) {
	const emailReg = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	return emailReg.test(email); //o test retorna true se encontrar o parametro pretentido
}

$(document).ready(function(){
  $('.signin--form').submit(function(event){
    event.preventDefault();

    const email = $('.signin--form input[name="email"]')
    const password = $('.signin--form input[name="password"]')

    if (email.val() === "" || password.val() === "") {
      $('.signin--error').css('display', 'block').html('<p class="message" style="color: #ce4043">Existem campos vazios</p>')
    } else if(!validateEmail(email.val())) {
      email.focus()
      $('.signin--error').css('display', 'block').html('<p class="message" style="color: #ce4043">Formato de e-mail incorrecto</p>')
    } else if(password.val().length < 6) {
      password.focus()
      $('.signin--error').css('display', 'block').html('<p class="message" style="color: #ce4043">Password necessita 6 caracteres</p>')
    } else {
      $('.signin--error').css('display', 'block').html('<p class="message" style="color: #56c473">Aguarde...</p>')
      $.ajax({
				url: './ajax/authuser',
				method: 'POST',
				dataType: 'JSON',
				data: {
          update: 1,
          email: email.val(),
          password: password.val()
				},
				success: function(response){
					if (response[0] !== 1) {
            $('.signin--error').css('display', 'block').html('<p class="message" style="color: #ce4043">'+response[1]+'</p>')
          } else {
            window.location.href = "/";
          }
        },
        error: function(response){
          console.log(response)
        }
			});
    }
  })
})