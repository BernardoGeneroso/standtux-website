function validateEmail(email) {
	const emailReg = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	return emailReg.test(email); //o test retorna true se encontrar o parametro pretentido
}

$(document).ready(function(){
  $('.signup--form').submit(function(event){
    event.preventDefault();

    const name = $('.signup--form input[name="name"]')
    const email = $('.signup--form input[name="email"]')
    const password = $('.signup--form input[name="password"]')

    if (name.val() === "" || email.val() === "" || password.val() === "") {
      $('.signup--error').css('display', 'block').html('<p class="message" style="color: #ce4043">Existem campos vazios</p>')
    } else if(!validateEmail(email.val())) {
      email.focus()
      $('.signup--error').css('display', 'block').html('<p class="message" style="color: #ce4043">Formato de e-mail incorrecto</p>')
    } else if(password.val().length < 6) {
      password.focus()
      $('.signup--error').css('display', 'block').html('<p class="message" style="color: #ce4043">Password necessita 6 caracteres</p>')
    } else {
      $('.signup--error').css('display', 'block').html('<p class="message" style="color: #56c473">Aguarde...</p>')
      $.ajax({
				url: './ajax/createuser',
				method: 'POST',
				dataType: 'JSON',
				data: {
          update: 1,
          name: name.val(),
          email: email.val(),
          password: password.val()
				},
				success: function(response){
					if (response[0] === 1) {
            $('.signup--error').css('display', 'block').html('<p class="message" style="color: #56c473">Bem vindo '+response[1]+'</p>')
          } else {
            $('.signup--error').css('display', 'block').html('<p class="message" style="color: #ce4043">'+response[1]+'</p>')
          }
        }
			});
    }
  })
})