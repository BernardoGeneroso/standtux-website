<?php 
  session_start();
  /**
   * Se a sessão estiver iniciada, o utilizador volta para a página principal
   */
  if (isset($_SESSION['user_id'])) {
    header('Location: /');
  }

  global $lastPage;
  /**
   * Vai buscar a ultima página para depois ao voltar para trás retornar a essa mesma página
   */
  if (isset($_GET['lastpage'])) {
    $lastPage.=$_GET['lastpage'];
  }
?>

<!DOCTYPE html>
<html lang="pt-PT">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <link rel="stylesheet" href="./assets/css/global.css">
  <link rel="stylesheet" href="./assets/css/signin/index.css">
  <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'>
  <link rel="icon" href="./assets/img/logos/siteicon.ico" type="image/x-icon">

  <title>Stand Tux - Entrar</title>
</head>
<body>
  <div class="signin--container">
    <div class="signin--box">
      <div class="signin--content">
        <img src="./assets/img/logos/logoStandTux.png" alt="logoStandTux">

        <form class="signin--form">
          <h3>Entrar</h3>

          <div class="signin--input">
            <span>
               <i class="fa fa-envelope"></i>
            </span>
            <input name="email" type="email" placeholder="E-mail">
          </div>
          <div class="signin--input">
            <span>
              <i class="fa fa-lock"></i>
            </span>
            <input name="password" type="password" placeholder="Password">
          </div>

          <span class="signin--error"></span>

          <button type="submit">Entrar</button>
        </form>
        
        <a href="/<?= ($lastPage === '/') ? '' : $lastPage ?>">
          <button  class="signin--comeback">
            <i class="fa fa-reply"></i>
            <span>Voltar atrás</span>
          </button>
        </a>
      </div>

      <a href="/signup">
        <button
          class="signin--create-account"
        > <i class="fa fa-user-plus"></i><span>Criar uma conta</span></button>
      </a>
    </div>
  </div>

  
  <script src="./assets/js/jquery-3.5.1.min.js"></script>
  <script src="./assets/js/signin/script.js"></script>
</body>
</html>