<footer class="page-footer font-small indigo">
  <div class="container text-center text-md-left">
    <div class="row">
      <div class="col-md-5 mx-auto d-flex flex-column align-items-center">
        <h5 class="font-weight-bold text-uppercase mt-3 mb-4">Informações</h5>
        <ul class="list-unstyled">
          <li>
          <i class="fa fa-envelope" aria-hidden="true"></i> suporte@standtux.pt
          </li>
          <li>
          </li>
          <li>
            <i class="fa fa-home" aria-hidden="true"></i> Estr. da Penha 139, 8005-139 Faro
          </li>
          <a href="/contacts">
            <li>
              Mais informações...
            </li>
          </a>
        </ul>
      </div>

      <hr class="clearfix w-100 d-md-none">
      <div class="col-md-5 mx-auto d-flex flex-column align-items-center">
        <h5 class="font-weight-bold text-uppercase mt-3 mb-4">Links das páginas</h5>
        <ul class="list-unstyled">
          <li>
            <a href="/">Home</a>
          </li>
          <li>
            <a href="/motorized">Loja</a>
          </li>
          <li>
            <a href="/team">Equipa</a>
          </li>
          <li>
            <a href="/contacts">Contactos</a>
          </li>
        </ul>

      </div>

      <hr class="clearfix w-100 d-md-none">
    </div>
  </div>

  <!-- Copyright -->
  <div class="footer-copyright text-center py-3">© 2020 Copyright:
    <a href="http://standtux.pt/"> Stand Tux</a>
  </div>

</footer>