<?php 
    session_start();
?>

<link rel="stylesheet" href="../assets/css/navbar/index.css">

<nav class="navbar navbar-expand-md navbar-light bg-light navbar-style">
    <a href="/" class="navbar-brand">
        <img src="../assets/img/logos/logoName.png" height="100">
    </a>
    <?php 
        if(isset($_SESSION['user_id'])) {
            echo '
            <div class="dropdown">
                <div class="avatar--container" id="dropdownMenuPerfil" data-toggle="dropdown">
                    <img src="'.$_SESSION['user_image'].'" id="image-user" alt="'.$_SESSION['user_name'].'">

                    <div>
                        <span>Bem vindo,</span>
                        <span>'.$_SESSION['user_name'].'</span>
                    </div>
                </div>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuPerfil">
                    <button class="dropdown-item" type="button" data-toggle="modal" data-target="#perfilMenu">Meu perfil</button>
                    <button class="dropdown-item" type="button" data-toggle="modal" data-target="#wishListMotorized">Lista de desejos</button>
                </div>
            </div>
            ';
        }
    ?>
    <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarCollapse">
        <div class="navbar-nav ml-auto">
            <a href="/" class="nav-item nav-link <?= ($header === '/') ? "active" : '' ?>">Home</a>
            <a href="/motorized" class="nav-item nav-link <?= ($header === 'motorized') ? "active" : '' ?>">Loja</a>
            <a href="/team" class="nav-item nav-link <?= ($header === 'team') ? "active" : '' ?>">Equipa</a>
            <a href="/contacts" class="nav-item nav-link <?= ($header === 'contacts') ? "active" : '' ?>">Contactos</a>
            <?php
                if (isset($_SESSION['user_admin']) && $_SESSION['user_admin'] == 1) {
                    $header = ($header === 'admin') ? "active" : '';
                    echo '<a href="/admin" class="nav-item nav-link '.$header.'">Painel Admin</a>';
                }
            ?>

            <?php 
                if (isset($_SESSION['user_id'])) {
                    echo '
                    <a href="/logout.php">
                        <i class="fa fa-power-off" id="fa-power-off" aria-hidden="true"></i>
                    </a>
                    ';
                } else {
                    $lastpage = $header;
                    echo '
                    <a href="/signin?lastpage='.$lastpage.'" class="nav-item nav-link">
                        <button type="button" class="btn btn-outline-danger spaceButton">Entrar</button>
                    </a>
                    <a href="/signup?lastpage='.$lastpage.'" class="nav-item nav-link">
                        <button type="button" class="btn btn-danger">Registar</button>
                    </a>
                    ';
                }
            ?>
        </div>
    </div>
</nav>