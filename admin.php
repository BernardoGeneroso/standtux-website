<?php 
  session_start();

  /**
   * Caso a sessão não seja admin, redireciona para a página principal
   */
  if ($_SESSION['user_admin'] != 1) {
    header('Location: /');
  }

  include_once('./db/db.php');
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSS & Icon  -->
    <link rel="stylesheet" href="./assets/css/global.css">
    <link rel="stylesheet" href="./assets/css/painel-admin/index.css">
    <link rel="stylesheet" href="./assets/css/toastr/toastr.min.css">
    <link rel="stylesheet" href="./assets/css/jquery-ui.min.css">
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="icon" href="./assets/img/logos/siteicon.ico" type="image/x-icon">

    <title>Stand Tux - Painel Admin</title>
  </head>
  <body>
    <div class="paineladmin">
      <header>
        <a href="/">
          <i class="fa fa-chevron-left"></i>
        </a>

        <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#addMotorized">Adicionar motorizada</button>
        <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#addBrand" style="display:none;">Adicionar marca</button>
      </header>

      <div id="tabs">
        <ul>
          <li><a href="#motorized-1">Motorizadas</a></li>
          <li><a href="#comment-home-2">Comentários(home)</a></li>
          <li><a href="#brand-image3">Marcas (imagens)</a></li>
        </ul>
        <div id="motorized-1">
          <table class="table table-striped">
            <thead>
              <tr>
                <th scope="col">Name</th>
                <th scope="col">Marca</th>
                <th scope="col">Preço</th>
                <th scope="col">Promotion</th>
                <th scope="col">Capacidade de combustível (l)</th>
                <th scope="col">Ano de fábrico</th>
                <th scope="col">Potência (kW)</th>
                <th scope="col">Cilindrada (cc)</th>
                <th scope="col">Peso (Kg)</th>
                <th scope="col">Stock</th>
                <th scope="col">Estado</th>
                <th scope="col">Editar</th>
              </tr>
            </thead>
            <tbody id="motorized_tbody">
              <?php
                /**
                 * Apresenta numa tabela todas as motorizadas
                 */
                function motorizedShow($row) {
                  $chechedConfirm = ($row[12]) ? 'Ativo' : 'Desativado';
                  echo '
                  <tr data-id="'.$row[0].'">
                    <th><a href="/product?id='.$row[0].'">'.$row[1].'</a></th>
                    <td>'.$row[2].'</td>
                    <td>'.$row[4].'€</td>
                    <td>'.$row[5].'%</td>
                    <td>'.$row[6].'</td>
                    <td>'.$row[7].'</td>
                    <td>'.$row[8].'</td>
                    <td>'.$row[9].'</td>
                    <td>'.$row[10].'</td>
                    <td>'.$row[11].'</td>
                    <td>'.$chechedConfirm.'</td>
                    <td>
                      <div class="paineladmin-edit">
                        <i class="fa fa-eye" title="'.$row[13].'"></i>
                        <i class="fa fa-edit" data-toggle="modal" data-target="#editMotorized"></i>
                        <i class="fa fa-trash"></i>
                      </div>
                    </td>
                  </tr>
                  ';
                }
                $sql = "SELECT * FROM motorized";

                $result = $conn->query($sql);

                  if ($row_cnt = $result->num_rows) {
                    while ($row = mysqli_fetch_row($result)) {
                      motorizedShow($row);
                    }
                  }
              ?>
            </tbody>
          </table>
        </div>
      <div id="comment-home-2">
        <table class="table table-striped">
          <thead> 
            <tr>
              <th scope="col">Name</th>
              <th scope="col">Comentário</th>
              <th scope="col">Classificação</th>
              <th scope="col">Aprovado</th>
              <th scope="col">Aprovação</th>
            </tr>
          </thead>
          <tbody id="rating_tbody">
            <?php
              /**
               * Apresenta numa tabela todos os comentários prontos para aprovação
               */
              function ratingComments($row) {
                $check = ($row[4] == 1) ? 'Sim' : 'Não';
                $aproved = ($row[4] != 1) ? '<i class="fa fa-check"></i>' : '<i class="fa fa-times"></i>';
                echo '
                <tr class="content" data-id="'.$row[0].'">
                  <td>'.$row[1].'</td>
                  <td>'.$row[2].'</td>
                  <td>'.$row[3].'</td>
                  <td>'.$check.'</td>
                  <td id="aproveCommentsInHome">
                    '.$aproved.'
                  </td>
                </tr>
                ';
              }

              $sql = "SELECT r.id_rating, u.name, r.description, r.rating, r.approved
              FROM rating AS r
              INNER JOIN user AS u
              ON r.id_user=u.id_u
              GROUP BY r.id_rating, r.approved
              ORDER BY r.approved DESC
              ";

              $result = $conn->query($sql);

              if ($row_cnt = $result->num_rows) {
                while ($row = mysqli_fetch_row($result)) {
                  ratingComments($row);
                }
              }
            ?>
          </tbody>
        </table>
      </div>

      <div id="brand-image3">
        <table class="table table-striped">
            <thead> 
              <tr>
                <th scope="col">Nome</th>
                <th scope="col">Imagem</th>
                <th scope="col">Apagar</th>
              </tr>
            </thead>
            <tbody id="brand_tbody">
              <?php
              /**
               * Apresenta uma lista com todas as imagens das marcas de motorizadas
               */
                function brands($row) {
                  echo '
                      <tr class="content">
                        <td>'.$row[0].'</td>
                        <td><img src="./assets/img/motorized/marcas/'.strtolower($row[1]).'.png" alt="'.$row[0].'" height="100"></td>
                        <td><i class="fa fa-times"></i></td>
                      </tr>
                      ';
                }

                $sql = "SELECT DISTINCT(brand), brand FROM `motorized`";

                $result = $conn->query($sql);

                  if ($row_cnt = $result->num_rows) {
                    while ($row = mysqli_fetch_row($result)) {
                      brands($row);
                    }
                  }
              ?>
            </tbody>
        </table>
      </div>


      <!-- Modal add motorized -->
      <div class="modal fade" id="addMotorized" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="addMotorizedLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="addMotorizedLabel">Adicionar motorizada</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
            <form>
              <div class="form-row">
                <div class="form-group col-md-6">
                  <label for="inputName">Nome</label>
                  <input type="text" class="form-control" id="inputName" placeholder="Nome">
                </div>
                <div class="form-group col-md-6">
                  <label for="inputBrand">Marca</label>
                  <input type="text" class="form-control" id="inputBrand" placeholder="Marca">
                </div>
              </div>

              <div class="form-row">
                <div class="form-group col-md-6">
                  <label for="inputPrice">Preço</label>
                  <input type="text" class="form-control" id="inputPrice" placeholder="Preço">
                </div>
                <div class="form-group col-md-6">
                  <label for="inputPromotion">Promoção</label>
                  <input type="text" class="form-control" id="inputPromotion" placeholder="Promoção">
                </div>
              </div>

              <div class="form-row">
                <div class="form-group col-md-6">
                  <label for="inputFuelCapacity">Capacidade de combustível (l)</label>
                  <input type="text" class="form-control" id="inputFuelCapacity" placeholder="Capacidade de combustível (l)">
                </div>
                <div class="form-group col-md-6">
                  <label for="inputYearManufacturing">Ano de fábrico</label>
                  <input type="text" class="form-control" id="inputYearManufacturing" placeholder="Ano de fábrico">
                </div>
              </div>

              <div class="form-row">
                <div class="form-group col-md-6">
                  <label for="inputPower">Potência (kW)</label>
                  <input type="text" class="form-control" id="inputPower" placeholder="Potência (kW)">
                </div>
                <div class="form-group col-md-6">
                  <label for="inputCylinder">Cilindrada (cc)</label>
                  <input type="text" class="form-control" id="inputCylinder" placeholder="Cilindrada (cc)">
                </div>
              </div>

              <div class="form-row">
                <div class="form-group col-md-6">
                  <label for="inputWeight">Peso (Kg)</label>
                  <input type="text" class="form-control" id="inputWeight" placeholder="Peso (Kg)">
                </div>
                <div class="form-group col-md-6">
                  <label for="inputStock">Stock</label>
                  <input type="text" class="form-control" id="inputStock" placeholder="Stock">
                </div>
              </div>
              
              <div class="mb-3">
                <label for="textareaDescription">Descrição</label>
                <textarea class="form-control" id="textareaDescription" rows="4" placeholder="Descrição" required></textarea>
              </div>

              <div class="form-group">
                <div class="form-check">
                  <input class="form-check-input" type="checkbox" id="active">
                  <label class="form-check-label" for="active">
                    Ativo
                  </label>
                </div>
              </div>
            </form>
            </div>
            <div class="modal-footer">
              <button type="button" id="createMotorizedCancel" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
              <button type="button" id="createMotorized" class="btn btn-success">Criar</button>
            </div>
          </div>
        </div>
      </div>
    </div>


    <!-- Modal edit motorized -->
    <div class="modal fade" id="editMotorized" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="editMotorizedLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="editMotorizedLabel">Editar motorizada</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
            <form>
              <div class="form-row">
                <div class="form-group col-md-6">
                  <label for="inputEditName">Nome</label>
                  <input type="text" class="form-control" id="inputEditName" placeholder="Nome">
                </div>
                <div class="form-group col-md-6">
                  <label for="inputEditBrand">Marca</label>
                  <input type="text" class="form-control" id="inputEditBrand" placeholder="Marca">
                </div>
              </div>

              <div class="form-row">
                <div class="form-group col-md-6">
                  <label for="inputEditPrice">Preço</label>
                  <input type="text" class="form-control" id="inputEditPrice" placeholder="Preço">
                </div>
                <div class="form-group col-md-6">
                  <label for="inputEditPromotion">Promoção</label>
                  <input type="text" class="form-control" id="inputEditPromotion" placeholder="Promoção">
                </div>
              </div>

              <div class="form-row">
                <div class="form-group col-md-6">
                  <label for="inputEditFuelCapacity">Capacidade de combustível (l)</label>
                  <input type="text" class="form-control" id="inputEditFuelCapacity" placeholder="Capacidade de combustível (l)">
                </div>
                <div class="form-group col-md-6">
                  <label for="inputEditYearManufacturing">Ano de fábrico</label>
                  <input type="text" class="form-control" id="inputEditYearManufacturing" placeholder="Ano de fábrico">
                </div>
              </div>

              <div class="form-row">
                <div class="form-group col-md-6">
                  <label for="inputEditPower">Potência (kW)</label>
                  <input type="text" class="form-control" id="inputEditPower" placeholder="Potência (kW)">
                </div>
                <div class="form-group col-md-6">
                  <label for="inputEditCylinder">Cilindrada (cc)</label>
                  <input type="text" class="form-control" id="inputEditCylinder" placeholder="Cilindrada (cc)">
                </div>
              </div>

              <div class="form-row">
                <div class="form-group col-md-6">
                  <label for="inputEditWeight">Peso (Kg)</label>
                  <input type="text" class="form-control" id="inputEditWeight" placeholder="Peso (Kg)">
                </div>
                <div class="form-group col-md-6">
                  <label for="inputEditStock">Stock</label>
                  <input type="text" class="form-control" id="inputEditStock" placeholder="Stock">
                </div>
              </div>

              <div class="custom-file">
                <input type="file" class="custom-file-input" id="editMotorizedFile">
                <label class="custom-file-label" for="editMotorizedFile">Imagem motorizada...</label>
              </div>

              <div id="error-message-img-motorized" style="display:none;"></div>
              
              <div class="mb-3">
                <label for="textareaEditDescription">Descrição</label>
                <textarea class="form-control" id="textareaEditDescription" rows="4" placeholder="Descrição" required></textarea>
              </div>

              <div class="form-group">
                <div class="form-check">
                  <input class="form-check-input" type="checkbox" id="activeEdit">
                  <label class="form-check-label" for="activeEdit">
                    Ativo
                  </label>
                </div>
              </div>
            </form>
            </div>
            <div class="modal-footer">
              <button type="button" id="editMotorizedCancel" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
              <button type="button" id="editMotorizedBtn" class="btn btn-success">Editar</button>
            </div>
          </div>
        </div>
      </div>
    </div>


    <!-- Modal add motorized -->
    <div class="modal fade" id="addBrand" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="addBrandLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="addBrandLabel">Adicionar marca</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
            <form>
              <div class="custom-file">
                <input type="file" class="custom-file-input" id="addBrandFile">
                <label class="custom-file-label" for="addBrandFile">Marca...</label>
              </div>
            </form>
            <div id="error-message-img-brand" style="display:none;">
              <p></p>
            </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-danger" id="addBrandCancel" data-dismiss="modal">Cancelar</button>            </div>
          </div>
        </div>
      </div>
    </div>


    <!-- Script's load -->
    <script src="./assets/js/jquery-3.5.1.min.js"></script>
    <script src="./assets/js/jquery-ui.min.js"></script>
    <script src="./assets/js/admin/script.js"></script>
    <script src="./assets/js/toastr/toastr.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>  
  </body>
</html>