<?php
  session_start();
  /**
   * Se a sessão estiver iniciada, o utilizador volta para a página principal
   */
  if (isset($_SESSION['user_id'])) {
    header('Location: /');
  }
?>

<!DOCTYPE html>
<html lang="pt-PT">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <link rel="stylesheet" href="./assets/css/global.css">
  <link rel="stylesheet" href="./assets/css/signup/index.css">
  <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'>
  <link rel="icon" href="./assets/img/logos/siteicon.ico" type="image/x-icon">

  <title>Stand Tux - SignUp</title>
</head>
<body>
<div class="signup--container">
    <div class="signup--box">
      <div class="signup--content">
        <img src="./assets/img/logos/logoStandTux.png" alt="logoStandTux">

        <form class="signup--form">
          <h3>Criar uma conta</h3>

          <div class="signup--input">
            <span>
               <i class="fa fa-user"></i>
            </span>
            <input name="name" type="text" placeholder="Nome">
          </div>
          <div class="signup--input">
            <span>
               <i class="fa fa-envelope"></i>
            </span>
            <input name="email" type="email" placeholder="E-mail">
          </div>
          <div class="signup--input">
            <span>
              <i class="fa fa-lock"></i>
            </span>
            <input name="password" type="password" placeholder="Password">
          </div>

          <span class="signup--error"></span>

          <button type="submit">Registar</button>
        </form>
      </div>

      <a href="/signin">
        <button
          class="signup--create-account"
        > <i class="fa fa-reply"></i><span>Já têm conta?</span></button>
      </a>
    </div>
  </div>


  <script src="./assets/js/jquery-3.5.1.min.js"></script>
  <script src="./assets/js/signup/script.js"></script>
  
</body>
</html>