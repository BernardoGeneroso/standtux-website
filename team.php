<?php
  /**
  * A página onde se encontra
  */
  $header = "team";
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSS & Icon  -->
    <link rel="stylesheet" href="./assets/css/global.css">
    <link rel="stylesheet" href="./assets/css/team/index.css">
    <link rel="stylesheet" href="./assets/css/toastr/toastr.min.css">
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="icon" href="./assets/img/logos/siteicon.ico" type="image/x-icon">

    <title>Stand Tux - Equipa</title>
  </head>
  <body>
    <!-- NavBar - Header -->
    <?php include './components/navbar.php'; ?>


    <!-- Team information -->
    <div class="infoteam">

      <!-- Store background -->
      <div class="store-motorized">
        <div class="header-container">
            <div class="header-img-holder">
              <span>Equipa</span>
            </div>
        </div>
      </div>

      <!-- Avatar -->
      <div class="infoteam--container">
        <div class="infoteam--avatar">
          <div class="infoteam--content">
          <img src="assets/img/team/avatar1.png" alt="" width="250" height="250">
              <h3>Diogo Agostinho</h3>
          </div>
          <div class="infoteam--content">
          <img src="assets/img/team/avatar2.png" alt="" width="250" height="250">
              <h3>Bernardo Generoso</h3>
          </div>
          <div class="infoteam--content">
          <img src="assets/img/team/avatar3.png" alt="" width="250" height="250">
              <h3>José Lima</h3>
          </div>
          <div class="infoteam--content">
          <img src="assets/img/team/avatar4.png" alt="" width="250" height="250">
              <h3>Steven Martins</h3>
          </div>
        </div>
      </div>

    </div>

    <!-- Footer -->
    <?php include './components/footer.php'; ?>

    <div>
      <i class="fa fa-arrow-up arrowUp" aria-hidden="true"></i>
    </div>

    <?php
      /**
       * Mostra as informações do utilizador e a lista de desejos caso a sessão estiver iniciada
       */
      if (isset($_SESSION['user_id'])){
        $id_user = $_SESSION['user_id'];
        echo '
        <!-- Wish list -->
        <div class="modal fade" id="wishListMotorized" tabindex="-1" role="dialog" aria-labelledby="wishListMotorizedModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="wishListMotorizedModalLabel">Lista de desejos</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">';
                  $sql = "SELECT fm.id_fav_motorized, m.name, m.image, m.price, m.stock
                          FROM fav_motorized AS fm
                          INNER JOIN motorized AS m
                          ON m.id_motorized=fm.id_motorized
                          WHERE id_user='$id_user'
                          ";
    
                  $result = $conn->query($sql);
    
                    if ($row_cnt = $result->num_rows) {
                      echo '<table class="table table-striped">
                      <thead>
                        <tr>
                          <th scope="col">Nome</th>
                          <th scope="col">Imagem</th>
                          <th scope="col">Preço</th>
                          <th scope="col">Stock</th>
                          <th scope="col">Remover</th>
                        </tr>
                      </thead>
                      <tbody id="listWishMotorized">';
                      while ($row = mysqli_fetch_row($result)) {
                        if ($row[4]  >= 1) {
                          $row[4] = "Disponível";
                        } else {
                          $row[4] = "Esgotado";
                        }
                        echo '
                        <tr class="content" data-id="'.$row[0].'">
                          <td>'.$row[1].'</td>
                          <td><img src="'.$row[2].'" alt="'.$row[1].'"></td>
                          <td>'.$row[3].'</td>
                          <td>'.$row[4].'</td>
                          <td id="removeWishListMotorized">
                            <i class="fa fa-times"></i>
                          </td>
                        </tr>
                        ';
                      }
                      echo '</tbody>
                      </table>';
                    } else {
                      echo '<div style="text-align:center;">Nenhuma motorizada na lista de desejos</div>';
                    }
                
              echo '</div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
              </div>
            </div>
          </div>
        </div>';
    
        echo '<!-- Perfil Menu -->
        <div class="modal fade" id="perfilMenu" tabindex="-1" role="dialog" aria-labelledby="perfilMenuModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="perfilMenuModalLabel">Perfil</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="perfil-user-image">
                    <input type="file" id="file-profile-user">
    
                    <img src="'.$_SESSION['user_image'].'" id="image-profile-user" alt="'.$_SESSION['user_name'].'">
                
                    <div id="error-message-img-profile" style="display:none;">
                      <p></p>
                    </div>
                </div>
    
                <form>
                  <div class="form-row">
                    <div class="form-group col-md-6">
                      <label for="inputNamePerfil">Nome</label>
                      <input type="text" class="form-control" id="inputNamePerfil" value="'.$_SESSION['user_name'].'" placeholder="Nome e apelido">
                    </div>
                    <div class="form-group col-md-6">
                      <label for="inputEmailPerfil">Email</label>
                      <input type="email" class="form-control" id="inputEmailPerfil" value="'.$_SESSION['user_email'].'" placeholder="Email">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputAddress">Endereço</label>
                    <input type="text" class="form-control" id="inputAddress" value="'.$_SESSION['user_address'].'" placeholder="Endereço">
                  </div>
                  <div class="form-row">
                    <div class="form-group col-md-4">
                      <label for="inputCity">Cidade</label>
                      <input type="text" class="form-control" id="inputCity" value="'.$_SESSION['user_city'].'" placeholder="Cidade">
                    </div>
                    <div class="form-group col-md-4">
                      <label for="inputNumberPhone">Número telemóvel</label>
                      <input type="text" class="form-control" id="inputNumberPhone" value="'.$_SESSION['user_mobile_phone'].'" maxlength="9" placeholder="Número telemóvel">
                    </div>
                    <div class="form-group col-md-2">
                      <label for="inputNIF">NIF</label>
                      <input type="text" class="form-control" id="inputNIF" value="'.$_SESSION['user_nif'].'" maxlength="9" placeholder="Contribuinte">
                    </div>
                    <div class="form-group col-md-2">
                      <label for="inputPostalCode">Código postal</label>
                      <input type="text" class="form-control" id="inputPostalCode" value="'.$_SESSION['user_postal_cod'].'" maxlength="8" placeholder="0000-000">
                    </div>
                  </div>
                </form>
                
                <button type="button" class="btn btn-success" id="saveDataProfileUser">Guardar alterações</button>';
    
                $sql = "SELECT id_rating FROM rating WHERE id_user='$id_user'";
    
                $result = $conn->query($sql);
    
                if (!$row_cnt = $result->num_rows) {
                  echo '<hr style="height:2px;border-width:0;background-color:whitesmoke">
                  <div class="profile-rating-stars">
                    <i data-position="1" class="fa fa-star"></i>
                    <i data-position="2" class="fa fa-star"></i>
                    <i data-position="3" class="fa fa-star"></i>
                    <i data-position="4" class="fa fa-star"></i>
                    <i data-position="5" class="fa fa-star"></i>
                  </div>
                   
                   <div class="comment-home-profile-user">
                      <div class="input-group mb-3">
                        <input type="text" class="form-control" placeholder="Comentário sobre a Stand Tux...">
                        <div class="input-group-append">
                          <button class="btn btn-outline-secondary" type="button" id="sendCommentProfileUser">Enviar</button>
                        </div>
                      </div>
                      <p style="display:none;"></p>
                    </div>';
                }
    
              echo '</div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
              </div>
            </div>
          </div>
        </div>';
      }
    ?>

    <!-- Script's load -->
    <script src="./assets/js/jquery-3.5.1.min.js"></script>
    <script src="./assets/js/toastr/toastr.min.js"></script>
    <script src="./assets/js/navbar/script.js"></script>
    <script src="./assets/js/team/script.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script> 
  </body>
</html>