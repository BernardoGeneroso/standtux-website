# README #

Stand Tux, site de vendas de motorizadas

### O site inclui: ###

* Registo de utilizadores
* Comentários em produtos
* Comentários de loja com sistema de classificação
* Listagem de motorizadas para venda
* Filtragem de produtos por marca, potência, preço e ano
* Pesquisa de motorizadas via barra de pesquisa
* Apresentação do produto, com nome, imagem, preço, promoção (se existir), capacidade de combustível, ano de fabrico, potência, cilindrada e peso
* Escolha entre adicionar ao carrinho ou á lista de desejos
* Sistema de listagem do carrinho de compras e lista de desejos
* Edição de perfil, ao qual pode editar a imagem, nome, email, endereço, cidade, número de telefone, NIF e código postal
* Painel admin, ao qual pode adicionar motorizadas e marcas, e pode aprovar ou desaprovar comentários

### Como é que eu configuro? ###

1. Entrar na pasta "db"
2. Abrir o ficheiro db.php e configurar
3. Voltar a página principal
4. Descarregar o ficheiro que termina com .sql

### Autores ###

* Bernardo Generoso
* Diogo Agostinho
* Steven Martins
* José Lima

### Com quem devo falar? ###

* O responsável pelo repositório

### Existe alguma demo? ###

* [Website base](https://standtux.pt/)
* [Website publicitário](https://standtux.joomla.com/)